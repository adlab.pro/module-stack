/* tslint:disable */
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { SubscriptionClient } from 'subscriptions-transport-ws';

import { IReactFeature } from './interfaces';

export function createApollo(module: IReactFeature) {
    const endpoint = module.context.APOLLO_URI || `http://${location.host}/graphql`;
    const link = (module.link || [] as any).concat([
        new HttpLink({ uri: endpoint }),
    ] as any);

    return new ApolloClient({
        cache: new InMemoryCache(),
        link: ApolloLink.from(link),
    });
}
