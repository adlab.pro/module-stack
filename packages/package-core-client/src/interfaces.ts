import { ApolloLink } from "apollo-link";
import { IFeature } from "@module-stack/core";

export interface IRoute {
    path: string;
    load?: () => Promise<any>;
    action?: () => any;
}

export interface IReactFeature extends IFeature {
    reducers?: any;
    link?: ApolloLink;
    routes?: IRoute[];
}

export interface IRenderPropsComponent {
    render(...data: any[]): any;
}
