import * as _ from "lodash";
import { ApolloLink } from "apollo-link";
import { schema, IPart, ICompletedSchema } from '@module-stack/core';

import { createStore } from './createStore';
import { createApollo } from './createApollo';
import { IReactFeature, IRoute } from './interfaces';

const ReducersPart: IPart = {
    composer: (reducers: any[]) => _.merge(...reducers),
};

const ApolloPart: IPart = {
    composer: (links: any[]) => ApolloLink.from(links),
};

const SchemaReactFeature = schema({
    routes: {},
    link: ApolloPart,
    reducers: ReducersPart,
});

export { createStore, IReactFeature, IPart, ReducersPart, SchemaReactFeature, createApollo };

