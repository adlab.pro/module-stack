import thunk from 'redux-thunk'
import logger from 'redux-logger'
import { createStore as store, Dispatch, applyMiddleware, AnyAction, compose, combineReducers } from 'redux';

import { IReactFeature } from './interfaces';

export function createStore(module: IReactFeature): any {
    const composeEnhancers: any = ((process.env.NODE_ENV === 'development' &&
        typeof window === 'object' &&
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose) as any;

    const reducers = combineReducers(module.reducers as any) as any;

    return (store as any)(reducers, {} as any, applyMiddleware(logger, thunk) as any);
}
