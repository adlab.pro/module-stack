import * as _ from "lodash";
import { Router } from "express";
import { mergeSchemas } from 'graphql-tools';
import { schema, ICompletedSchema, IPart } from '@module-stack/core';

import { IGraphqlFeature } from './interfaces';

const ObjectPart: IPart = {
    composer: objects => _.merge(...objects),
}

const GraphqlPart: IPart = {
    composer: schemas => mergeSchemas({ schemas }),
}

const RouterPart: IPart = {
    composer: routes => _.isEmpty(routes)
        ? Router()
        : _.reduce(routes, (router, instance) => router.use(instance), Router()),
}

const SchemaGraphqlFeature = schema({
    middlewares: {},
    mocks: ObjectPart,
    router: RouterPart,
    options: ObjectPart,
    context: ObjectPart,
    schema: GraphqlPart,
    schemaDirectives: ObjectPart,
});

export { IGraphqlFeature, SchemaGraphqlFeature };
