import { GraphQLSchema } from 'graphql';
import { RequestHandler } from 'express';
import { IMiddleware } from 'graphql-middleware';
import { Options as ServerOptions } from 'graphql-yoga';
import { schema, IPart, IFeature } from '@module-stack/core';

export interface IGraphqlFeature extends IFeature {
    mocks?: any;
    context?: any;
    schemaDirectives?: any;
    schema?: GraphQLSchema;
    router?: RequestHandler;
    options?: ServerOptions;
    middlewares?: IMiddleware[];
}
