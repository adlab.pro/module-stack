import * as _ from "lodash";
import { schema, ICompletedSchema } from '@module-stack/core';

import { IHemeraFeature, IAbstractPattern, IHemeraAction, IHemeraMiddleware, IHemeraReply } from './interfaces';

const SchemaHemeraFeature = schema({
    actions: {},
    config: {
        composer: (parts: any[]) => _.merge(...parts),
    },
});

export { IHemeraFeature, IAbstractPattern, IHemeraAction, SchemaHemeraFeature, IHemeraMiddleware, IHemeraReply };

