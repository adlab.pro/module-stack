import { AuthenticationClient, ManagementClient, UsersManager, DatabaseAuthenticator, OAuthAuthenticator } from 'auth0';
import {  IAuth0Service } from '../interfaces';

export class Auth0Service implements IAuth0Service {
    auth: any;
    settings: any;
    management: any;

    constructor({ domain, clientId, clientSecret }) {
        this.settings = { domain, clientId, clientSecret };

        this.auth = new AuthenticationClient({ 
            domain, 
            clientId, 
            clientSecret,
            scope: "openid app_metadata user_metadata",
        });
        this.management = new ManagementClient({ 
            domain, 
            clientId, 
            clientSecret, 
            scope: "read:users write:users" 
        });
    }

    async signInLoginPassword(username: String, password: String) {
        const result = await this.auth.database.signIn({ username, password });
        return this.getProfile(result.access_token);
    }

    async signInOAuth(access_token: String, connection: String) {
        const result = await this.auth.oauth.socialSignIn({ access_token, connection });
        return this.getProfile(result.access_token);
    }

   async getProfile(accessToken: String) {
        const result = await this.auth.getProfile(accessToken);
        return result;
    }

    async getPublicProfile(id: String) {
        try {
            const result = await this.management.getUser({ id });
            return result;
        } catch(e) {
            console.log(e)
            return {}
        }
    }
}
