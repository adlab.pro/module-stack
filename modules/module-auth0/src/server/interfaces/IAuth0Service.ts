export interface IAuth0Service {
    getPublicProfile(_id: string): any;
    getProfile(access_token: string): Promise<any>;
    signInOAuth(access_token: string, connection: string): Promise<any>;
    signInLoginPassword(username: string, password: string): Promise<any>;
}
