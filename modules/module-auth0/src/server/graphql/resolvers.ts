import { IAuth0Service } from '../interfaces';

export interface IAuth0FeatureContext {
    Auth0Service: IAuth0Service;
}

export const resolvers = {
    Query: {
        getPublicProfile: (root, { _id }, context: IAuth0FeatureContext) => context.Auth0Service.getPublicProfile(_id),
        getProfile: (root, { access_token }, context: IAuth0FeatureContext) => context.Auth0Service.getProfile(access_token),
    },
    Mutation: {
        signInOAuth: (root, { access_token, connection }, context: IAuth0FeatureContext) => context.Auth0Service.signInOAuth(access_token, connection),
        signInLoginPassword: (root, { username, password }, context: IAuth0FeatureContext) => context.Auth0Service.signInLoginPassword(username, password),
    },
};