export const types = `
    type Auth0Metadata {
        role: String
    }

    type Auth0UserMetadata {
        language: String
    }

    type Auth0Identity {
        user_id: String
        isSocial: String
        provider: String
        connection: String
    }

    type Auth0User {
        sub: String
        name: String
        email: String
        user_id: String
        picture: String
        clientID: String
        nickname: String
        updated_at: String
        created_at: String
        email_verified: Boolean
        identities:[Auth0Identity]
        app_metadata: Auth0Metadata
        user_metadata: Auth0UserMetadata
    }

    type Query {
        getPublicProfile(_id: String): Auth0User
        getProfile(access_token: String!): Auth0User!
    }

    type Mutation {
        signInOAuth(access_token: String!, connection: String!): Auth0User
        signInLoginPassword(username: String!, password: String!): Auth0User
    }
`;