import { feature } from '@module-stack/core';
import { IGraphqlFeature } from '@module-stack/core-server';

import { schema } from './graphql';
import { Auth0Service } from './services';

export const Auth0Feature = settings => feature<IGraphqlFeature>({
    schema,
    context: {
        Auth0Service: new Auth0Service(settings),
     }
});
