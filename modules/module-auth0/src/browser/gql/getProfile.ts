import gql from 'graphql-tag';

export const PROFILE_QUERY = gql`
    query profile($access_token: String!) {
        getProfile(access_token: $access_token) {
            sub
            name
            email
            user_id
            picture
            clientID
            nickname
            updated_at
            created_at
            email_verified
            app_metadata {
                role
            }
            user_metadata {
                language
            }
        }
    }
`;