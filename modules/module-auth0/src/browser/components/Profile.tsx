import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { PROFILE_QUERY } from '../gql';

export interface IMenuProps {
    access_token: String;
    render(...props: any[]): any;
}

export function Profile(props: IMenuProps) {
    return (
        <React.Fragment>
            <Query skip={!props.access_token} variables={{ access_token: props.access_token }} query={PROFILE_QUERY}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getProfile'), ...result })}
            </Query>
        </React.Fragment>
    );
}
