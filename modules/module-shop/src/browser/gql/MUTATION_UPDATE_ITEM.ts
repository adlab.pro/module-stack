import gql from 'graphql-tag';

export const MUTATION_UPDATE_ITEM = gql`
    mutation editShopItem($_id: ID!, $input: IShopItem!) {
        updateShopItem(_id: $_id, input: $input) {
            _id
            name
       }
    }
`;