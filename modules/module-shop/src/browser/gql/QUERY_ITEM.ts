import gql from 'graphql-tag';

export const QUERY_ITEM = gql`
    query item($_id: ID!) {
        getShopItem(_id: $_id) {
            _id
            qty
            name
            price
            image
            images
            category
            description
        }
    }
`;