import gql from 'graphql-tag';

export const MUTATION_ADD_ITEM = gql`
    mutation addShopItem($input: IShopItem!) {
        createShopItem(input: $input) {
            _id
            name
       }
    }
`;