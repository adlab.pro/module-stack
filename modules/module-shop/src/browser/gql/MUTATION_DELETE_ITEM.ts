import gql from 'graphql-tag';

export const MUTATION_DELETE_ITEM = gql`
    mutation deleteShopItem($_id: ID!) {
        removeShopItem(_id: $_id)
    }
`;