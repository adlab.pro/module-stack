import gql from 'graphql-tag';

export const QUERY_ITEMS = gql`
    query items($query: IShopItemsQuery) {
        getShopItems(query: $query) {
            page
            total
            limit
            pages
            docs {
                _id
                qty
                name
                price
                image
                images
                category
                shortcut
            }
        }
    }
`;