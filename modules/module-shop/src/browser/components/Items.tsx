import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { QUERY_ITEMS } from '../gql';

export interface IShopItemsProps {
    query: any;
    render(...props: any[]): any;
}

export function Items(props: IShopItemsProps) {
    return (
        <React.Fragment>
            <Query variables={{ query: props.query }} query={QUERY_ITEMS}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getShopItems'), loading: data.loading, ...result })}
            </Query>
        </React.Fragment>
    );
}
