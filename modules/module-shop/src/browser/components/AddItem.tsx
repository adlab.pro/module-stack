import _ from 'lodash';
import * as React from 'react';
import { Mutation } from 'react-apollo';

import { MUTATION_ADD_ITEM } from '../gql';

export interface IAddItemsProps {
    render(...props: any[]): any;
}

export function AddItem(props: IAddItemsProps) {    
    return (
        <React.Fragment>
            <Mutation mutation={MUTATION_ADD_ITEM}>{(...args) => props.render(...args)}</Mutation>
        </React.Fragment>
    );
}
