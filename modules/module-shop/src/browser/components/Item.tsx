import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { QUERY_ITEM } from '../gql';

export interface IShopItemProps {
    _id: string;
    render(...props: any[]): any;
}

export function Item(props: IShopItemProps) {
    return (
        <React.Fragment>
            <Query variables={{ _id: props._id }} query={QUERY_ITEM}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getShopItem'), loading: data.loading, ...result })}
            </Query>
        </React.Fragment>
    );
}
