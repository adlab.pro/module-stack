import _ from 'lodash';
import * as React from 'react';
import { Mutation } from 'react-apollo';

import { MUTATION_DELETE_ITEM } from '../gql';

export interface IDeleteItemsProps {
    render(...props: any[]): any;
}

export function DeleteItem(props: IDeleteItemsProps) {    
    return (
        <React.Fragment>
            <Mutation mutation={MUTATION_DELETE_ITEM}>{(...args) => props.render(...args)}</Mutation>
        </React.Fragment>
    );
}
