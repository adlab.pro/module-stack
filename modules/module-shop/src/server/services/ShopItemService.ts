import * as _ from 'lodash';
import { ShopItem, IShopItemDocument } from '../models';
import { IShopItem, IShopItemService } from '../interfaces';

export class ShopItemService implements IShopItemService {
    get(_id: any) {
        return ShopItem.findOne({ _id }).exec();
    }

    remove(_id: any) {
        return ShopItem.remove({ _id }).exec();
    }

    fing(args: any) {
        const { brand, category, tags, author, search, page = 1, limit = 15 } = args;
        const options = { page, limit } as any;
        const query = { status: { $nin: ['status.archived', 'status.blocked'] } } as any;

        if (brand) query.brand = brand;
        if (author) query.merchant = author;
        if (category) query.category = category;
        if (search) query.$text = { $search: search };
        if (args.sort && !_.isEmpty(args.sort)) options.sort = { [args.sort.field]: args.sort.value };

        return ShopItem.paginate(query, options);
    }

    create(data: IShopItem) {
        return ShopItem.create(data);
    }

    async update(_id: String, data: IShopItem) {
        const updated = await ShopItem.update({ _id }, data);
        return this.get(_id);
    }
}
