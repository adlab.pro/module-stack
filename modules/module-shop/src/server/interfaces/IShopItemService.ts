import { IShopItem } from './IShopItem';

export interface IAbstractRepository<T> {
    create(data: T): Promise<T>;
    get(_id: String): Promise<T>;
    fing(query: any): Promise<any>;
    remove(_id: String): Promise<Boolean>;
    update(_id: String, data: T): Promise<T>;
}

export interface IShopItemService extends IAbstractRepository<IShopItem> {}
