export interface IShopItem {
    _id: any;
    qty: number;
    price: number;
    name: string;
    tags: string[];
    author: string;
    deleted: boolean;
    images: string[];
    category: string;
    updatedAt: string;
    createdAt: string;
    published: Boolean;
    description: string;
}
