import * as _ from 'lodash';

export const resolvers = {
    Query: {
        getShopItem: (root, { _id }, context) => context.ShopItemService.get(_id),
        getShopItems: (root, args, context) => context.ShopItemService.fing(args.query || {}),
    },
    Mutation: {
        removeShopItem: (root, { _id }, context) => context.ShopItemService.remove(_id),
        createShopItem: (root, { input }, context) => context.ShopItemService.create(input),
        updateShopItem: (root, { _id, input }, context) => context.ShopItemService.update(_id, input),
    },
    ShopItem: {
        image: root => _.get(root, 'images[0]'),
    },
};