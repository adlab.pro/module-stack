export const types = `
    type ShopItem {
        _id: ID!
        qty: Int
        price: Float
        name: String
        tags: [String]
        image: String
        author: String
        shortcut: String
        deleted: Boolean
        images: [String]
        category: String
        updatedAt: String
        createdAt: String
        published: Boolean
        description: String
    }

    input IShopItem {
        _id: ID
        qty: Int
        price: Float
        name: String!
        tags: [String]
        author: String
        shortcut: String
        deleted: Boolean
        images: [String]
        category: String
        updatedAt: String
        createdAt: String
        published: Boolean
        description: String!
    }

    input IShopItemsQuery {
        page: Int
        limit: Int
        tag: String
        author: String
        category: String
    }

    type ShopProductsList {
        page: Int
        total: Int
        limit: Int
        pages: Int
        docs: [ShopItem]
    }

    type Query {
        getShopItem(_id: ID!): ShopItem
        getShopItems(query: IShopItemsQuery): ShopProductsList
    }

    type Mutation {
        removeShopItem(_id: ID!): Boolean
        createShopItem(input: IShopItem!): ShopItem
        updateShopItem(_id: ID!, input: IShopItem!): ShopItem
    }
`;