import * as Paginate from 'mongoose-paginate';
import * as Timestamp from 'mongoose-timestamp';
import { Schema, Document, model , Model} from 'mongoose';

import { IShopItem } from '../interfaces';

export interface IShopItemDocument extends IShopItem, Document {}

const ShopItemSchema = new Schema({
    tags: [{ type: String }],
    price: { type: Number },
    images: [{ type: String }],
    category: { type: String },
    shortcut: { type: String },
    name: { type: String, required: true },
    description: { type: String, required: true },
    qty: { type: Number, require: true, default: -1 },
    deleted: { type: Boolean, required: true, default: false },
    published: { type: Boolean, required: true, default: true },
    author: { type: String, required: true, default: '__guest__' },
});

ShopItemSchema.plugin(Paginate);
ShopItemSchema.plugin(Timestamp);

export const ShopItem = model<IShopItemDocument>('ShopItem', ShopItemSchema) as any;
