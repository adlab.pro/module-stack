import { feature } from '@module-stack/core';
import { IGraphqlFeature } from '@module-stack/core-server';

import { schema } from './graphql';
import { ShopItemService } from './services';

export const ShopFeature = feature<IGraphqlFeature>({
    schema,
    context: {
        ShopItemService: new ShopItemService(),
     }
});
