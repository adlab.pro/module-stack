export interface IMenuElement {
    type: String;
    href: String;
    title: String;
    children: [IMenuElement];
}

export interface IMenu {
    name: String;
    active: Boolean;
    elements: [IMenuElement];
}
