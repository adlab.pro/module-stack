import { Schema, Document, model , Model} from 'mongoose';
import * as Timestamp from 'mongoose-timestamp';

import { IMenu } from '../interfaces';

export interface IMenuDocument extends IMenu, Document {}

const ElementSchema = new Schema({
    type: { type: String },
    href: { type: String },
    title: { type: String, require: true },
});

// Workaround for recursive schema
ElementSchema.add({
    children: [ElementSchema],
});

const MenuSchema = new Schema({
    elements: [{ type: ElementSchema }],
    name: { type: String, required: true },
    active: { type: Boolean, require: true, default: true } ,
});

MenuSchema.plugin(Timestamp);

export const Menu = model<IMenuDocument>('Menu', MenuSchema);
