import { Menu, IMenuDocument } from '../models';
import { IMenuService, IMenu } from '../interfaces';

export class MenuService implements IMenuService {
    get(_id: any) {
        return Menu.findOne({ _id }).exec();
    }

    remove(_id: any) {
        return Menu.remove({ _id }).exec();
    }

    fing(query: any) {
        return Menu.find(query).exec();
    }

    create(data: IMenu) {
        return Menu.create(data);
    }

    async update(_id: String, data: IMenu) {
        const result = await Menu.update({ _id }, data);
        return this.get(_id);
    }
}
