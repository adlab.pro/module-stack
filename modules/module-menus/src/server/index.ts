import { feature } from '@module-stack/core';
import { IGraphqlFeature } from '@module-stack/core-server';

import { schema } from './graphql';
import { MenuService } from './services';

export const MenuFeature = feature<IGraphqlFeature>({
    schema,
    context: {
        MenuService: new MenuService(),
     }
});
