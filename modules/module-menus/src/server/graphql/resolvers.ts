export const resolvers = {
    Query: {
        getMenus: (root, args, context) => context.MenuService.fing({}),
        getMenu: (root, { _id }, context) => context.MenuService.get(_id),
    },
    Mutation: {
        removeMenu: (root, { _id }, context) => context.MenuService.remove(_id),
        createMenu: (root, { menu }, context) => context.MenuService.create(menu),
        updateMenu: (root, { _id, menu }, context) => context.MenuService.update(_id, menu),
    },
};