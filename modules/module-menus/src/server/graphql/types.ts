export const types = `
    type MenuElement {
        type: String
        href: String
        title: String
        children: [MenuElement]
    }

    type Menu {
        _id: ID!
        name: String
        active: Boolean
        updatedAt: String
        createdAt: String
        elements: [MenuElement]
    }

    input IMenu {
        name: String
        elements: [IMenuElement]
    }

    input IMenuElement {
        type: String
        href: String
        title: String
        children: [IMenuElement]
    }

    type Query {
        getMenus: [Menu]
        getMenu(_id: String): Menu
    }

    type Mutation {
        removeMenu(_id: ID!): Boolean
        createMenu(menu: IMenu!): Menu
        updateMenu(_id: ID!, menu: IMenu!): Menu
    }
`;