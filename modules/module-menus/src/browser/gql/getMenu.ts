import gql from 'graphql-tag';

export const ELEMENT_FRAGMENT = `
fragment MenuElementFragment on MenuElement {
    href
    type
    title
}
`;

export const MENU_QUERY = gql` 
    ${ELEMENT_FRAGMENT}

    query menu($_id: String!) {
        getMenu(_id: $_id) {
            _id
            name
            elements {
                ...MenuElementFragment
                children {
                    ...MenuElementFragment
                    children {
                        ...MenuElementFragment
                    }
                }
            }
        }
    }
`;
