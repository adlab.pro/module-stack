import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { MENU_QUERY } from '../gql';

export interface IMenuProps {
    _id: String;
    render(...props: any[]): any;
}

export function Menu(props: IMenuProps) {
    return (
        <React.Fragment>
            <Query variables={{ _id: props._id }} query={MENU_QUERY}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getMenu'), ...result })}
            </Query>
        </React.Fragment>
    );
}
