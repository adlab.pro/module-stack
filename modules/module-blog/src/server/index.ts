import { feature } from '@module-stack/core';
import { IGraphqlFeature } from '@module-stack/core-server';

import { schema } from './graphql';
import { PostService } from './services';

export const PostFeature = feature<IGraphqlFeature>({
    schema,
    context: {
        PostService: new PostService(),
     }
});
