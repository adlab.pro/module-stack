export interface IPost {
    _id: any;
    title: string;
    tags: string[];
    author: string;
    content: string;
    deleted: string;
    category: string;
    updatedAt: string;
    createdAt: string;
    published: Boolean;
}
