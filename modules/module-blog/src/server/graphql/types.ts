export const types = `
    type Post {
        _id: ID!
        title: String!
        tags: [String]
        author: String
        content: String!
        deleted: Boolean
        shortcut: String
        category: String
        updatedAt: String
        createdAt: String
        published: Boolean
    }

    input IPost {
        _id: ID
        title: String!
        tags: [String]
        author: String
        shortcut: String
        content: String!
        category: String
        published: Boolean
    }

    input IPostsQuery {
        page: Int
        limit: Int
        tag: String
        author: String
        category: String
    }

    type Query {
        getPost(_id: ID!): Post
        getPosts(query: IPostsQuery): [Post]
    }

    type Mutation {
        removePost(_id: ID!): Boolean
        createPost(post: IPost!): Post
        updatePost(_id: ID!, post: IPost!): Post
    }
`;