export const resolvers = {
    Query: {
        getPost: (root, { _id }, context) => context.PostService.get(_id),
        getPosts: (root, { query }, context) => context.PostService.fing(query),
    },
    Mutation: {
        removePost: (root, { _id }, context) => context.PostService.remove(_id),
        createPost: (root, { post }, context) => context.PostService.create(post),
        updatePost: (root, { _id, post }, context) => context.PostService.update(_id, post),
    },
};