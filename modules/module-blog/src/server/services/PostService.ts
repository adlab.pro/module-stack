import { Post, IPostDocument } from '../models';
import { IPost, IPostService } from '../interfaces';

export class PostService implements IPostService {
    get(_id: any) {
        return Post.findOne({ _id }).exec();
    }

    remove(_id: any) {
        return Post.remove({ _id }).exec();
    }

    fing(query: any) {
        return Post.find(query).exec();
    }

    create(data: IPost) {
        return Post.create(data);
    }

    async update(_id: String, data: IPost) {
        const updated = await Post.update({ _id }, data);
        return this.get(_id);
    }
}
