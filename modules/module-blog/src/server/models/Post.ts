import { Schema, Document, model , Model} from 'mongoose';
import * as Timestamp from 'mongoose-timestamp';

import { IPost } from '../interfaces';

export interface IPostDocument extends IPost, Document {}

const PostSchema = new Schema({
    shortcut: String,
    tags: [{ type: String }],
    category: { type: String },
    title: { type: String, required: true },
    content: { type: String, required: true },
    deleted: { type: Boolean, required: true, default: false },
    published: { type: Boolean, required: true, default: true },
    author: { type: String, required: true, default: '__guest__' },
});

PostSchema.plugin(Timestamp);

export const Post = model<IPostDocument>('Post', PostSchema);
