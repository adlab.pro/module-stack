import _ from 'lodash';
import * as React from 'react';
import { Mutation } from 'react-apollo';

import { MUTATION_ADD_POST } from '../gql';

export interface IAddPostProps {
    render(...props: any[]): any;
}

export function AddPost(props: IAddPostProps) {    
    return (
        <React.Fragment>
            <Mutation mutation={MUTATION_ADD_POST}>{(...args) => props.render(...args)}</Mutation>
        </React.Fragment>
    );
}
