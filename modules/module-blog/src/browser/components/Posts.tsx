import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { QUERY_POSTS } from '../gql';

export interface IShopItemsProps {
    query: any;
    render(...props: any[]): any;
}

export function Posts(props: IShopItemsProps) {
    return (
        <React.Fragment>
            <Query variables={{ query: props.query }} query={QUERY_POSTS}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getPosts'), loading: data.loading, ...result })}
            </Query>
        </React.Fragment>
    );
}
