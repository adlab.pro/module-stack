import _ from 'lodash';
import * as React from 'react';
import { Mutation } from 'react-apollo';

import { MUTATION_UPDATE_POST } from '../gql';

export interface IEditPostProps {
    render(...props: any[]): any;
}

export function EditPost(props: IEditPostProps) {
    return (
        <React.Fragment>
            <Mutation mutation={MUTATION_UPDATE_POST}>{(...args) => props.render(...args)}</Mutation>
        </React.Fragment>
    );
}
