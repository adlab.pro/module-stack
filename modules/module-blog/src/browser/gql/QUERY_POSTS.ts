import gql from 'graphql-tag';

export const QUERY_POSTS = gql`
    query posts($query: IPostsQuery) {
        getPosts(query: $query) {
            _id
            title
            author
            category
            shortcut
        }
    }
`;