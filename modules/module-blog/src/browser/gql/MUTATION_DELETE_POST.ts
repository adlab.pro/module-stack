import gql from 'graphql-tag';

export const MUTATION_DELETE_POST = gql`
    mutation deletePost($_id: ID!) {
        removePost(_id: $_id)
    }
`;