import gql from 'graphql-tag';

export const MUTATION_UPDATE_POST = gql`
    mutation editPost($_id: ID!, $post: IPost!) {
        updatePost(_id: $_id, post: $post) {
            _id
            title
       }
    }
`;