import gql from 'graphql-tag';

export const MUTATION_ADD_POST = gql`
    mutation addShopItem($post: IPost!) {
        createPost(post: $post) {
            _id
            title
            content
       }
    }
`;