import gql from 'graphql-tag';

export const QUERY_POST = gql`
    query post($_id: ID!) {
        getPost(_id: $_id) {
            _id
            title
            author
            content
            category
        }
    }
`;