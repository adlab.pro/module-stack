import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { QUERY_CATEGORIES } from '../gql';

export interface ICommentsProps {
    refId: any;
    target: any;
    render(...props: any[]): any;
}

export function Comments(props: ICommentsProps) {
    return (
        <React.Fragment>
            <Query variables={{ target: props.target, ref: props.refId }} query={QUERY_CATEGORIES}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getComments'), loading: data.loading, ...result })}
            </Query>
        </React.Fragment>
    );
}
