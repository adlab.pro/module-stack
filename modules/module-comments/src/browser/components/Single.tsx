import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { QUERY_CATEGORY } from '../gql';

export interface ICommentProps {
    _id: string;
    render(...props: any[]): any;
}

export function Comment(props: ICommentProps) {
    return (
        <React.Fragment>
            <Query variables={{ _id: props._id }} query={QUERY_CATEGORY}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getComment'), loading: data.loading, ...result })}
            </Query>
        </React.Fragment>
    );
}
