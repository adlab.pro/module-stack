import gql from 'graphql-tag';

export const MUTATION_ADD = gql`
    mutation addComment($input: IComment!) {
        createComment(input: $input) {
            _id
            ref
            text
            author
            target
            deleted
            updatedAt
            createdAt
       }
    }
`;