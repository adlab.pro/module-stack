import gql from 'graphql-tag';

export const QUERY_CATEGORIES = gql`
    query list($ref: ID, $target: ID!) {
        getComments(ref: $ref, target: $target) {
            _id
            ref
            text
            author
            target
            deleted
            updatedAt
            createdAt
        }
    }
`;