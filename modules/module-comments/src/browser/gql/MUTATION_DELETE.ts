import gql from 'graphql-tag';

export const MUTATION_DELETE = gql`
    mutation deleteComment($_id: ID!) {
        removeComment(_id: $_id)
    }
`;