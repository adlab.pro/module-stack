import gql from 'graphql-tag';

export const QUERY_CATEGORY = gql`
    query category($_id: ID!) {
        getComment(_id: $_id) {
            _id
            ref
            text
            author
            target
            deleted
            updatedAt
            createdAt
        }
    }
`;