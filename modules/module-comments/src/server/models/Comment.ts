import { Schema, Document, model , Model} from 'mongoose';
import * as Timestamp from 'mongoose-timestamp';

import { IComment } from '../interfaces';

export interface ICommentDocument extends IComment, Document {}

const CommentSchema = new Schema({
    ref: { type: String, required: true },
    text: { type: String, required: true },
    target: { type: String, required: true },
    deleted: { type: Boolean, required: true, default: false },
    author: { type: String, required: true, default: '__guest__' },
});

CommentSchema.plugin(Timestamp);

export const Comment = model<ICommentDocument>('Comment', CommentSchema);
