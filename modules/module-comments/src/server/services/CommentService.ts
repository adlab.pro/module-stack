import { Comment, ICommentDocument } from '../models';
import { IComment, ICommentService } from '../interfaces';

export class CommentService implements ICommentService {
    get(_id: any) {
        return Comment.findOne({ _id }).exec();
    }

    remove(_id: any) {
        return Comment.remove({ _id }).exec();
    }

    fing({ target, ref }: any) {
        const query = { target } as any;
        if (ref) query.ref = ref; 
        return Comment.find(query).exec();
    }

    create(data: IComment) {
        return Comment.create(data);
    }
}
