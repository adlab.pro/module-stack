import { feature } from '@module-stack/core';
import { IGraphqlFeature } from '@module-stack/core-server';

import { schema } from './graphql';
import { CommentService } from './services';

export const CommentFeature = feature<IGraphqlFeature>({
    schema,
    context: {
        CommentService: new CommentService(),
     }
});
