export const resolvers = {
    Query: {
        getComments: (root, { ref, target }, context) => context.CommentService.fing({ ref, target }),
        getComment: (root, { _id }, context) => context.CommentService.get(_id),
    },
    Mutation: {
        removeComment: (root, { _id }, context) => context.CommentService.remove(_id),
        createComment: (root, { comment }, context) => context.CommentService.create(comment),
    },
};