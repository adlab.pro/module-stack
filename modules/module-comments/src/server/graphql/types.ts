export const types = `
    type Comment {
        _id: ID!
        ref: String
        text: String
        author: String
        target: String
        deleted: Boolean
        updatedAt: String
        createdAt: String
    }

    input IComment {
        ref: String!
        text: String!
        author: String!
        target: String!
    }

    type Query {
        getComment(_id: String): Comment
        getComments(target: ID!, ref: ID): [Comment]
    }

    type Mutation {
        removeComment(_id: ID!): Boolean
        createComment(comment: IComment!): Comment
    }
`;