import { IComment } from './IComment';

export interface IAbstractRepository<T> {
    create(data: T): Promise<T>;
    get(_id: String): Promise<T>;
    fing(query: any): Promise<T[]>;
    remove(_id: String): Promise<Boolean>;
    update?(_id: String, data: T): Promise<T>;
}

export interface ICommentService extends IAbstractRepository<IComment> {}
