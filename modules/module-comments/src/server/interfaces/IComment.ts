export interface IComment {
    _id: any;
    ref: String;
    text: String;
    author: String;
    target: String;
    deleted: Boolean;
    updatedAt: String;
    createdAt: String;
}
