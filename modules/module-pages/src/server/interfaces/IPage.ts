export interface IPage {
    url: String;   
    title: String;   
    content: String;   
    createdAt: String;   
    updatedAt: String;   
    published: Boolean;   
}
