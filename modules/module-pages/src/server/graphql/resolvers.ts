export const resolvers = {
    Query: {
        getPages: (root, args, context) => context.PageService.fing({}),
        getPage: (root, { _id }, context) => context.PageService.get(_id),
    },
    Mutation: {
        removePage: (root, { _id }, context) => context.PageService.remove(_id),
        createPage: (root, { page }, context) => context.PageService.create(page),
        updatePage: (root, { _id, page }, context) => context.PageService.update(_id, page),
    },
};