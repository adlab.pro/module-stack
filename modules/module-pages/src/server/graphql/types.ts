export const types = `
    type Page {
        _id: ID!
        url: String
        title: String
        content: String
        updatedAt: String
        createdAt: String
    }

    input IPage {
        _id: ID
        url: String
        title: String
        content: String
    }

    type Query {
        getPages: [Page]
        getPage(_id: ID!): Page
    }

    type Mutation {
        removePage(_id: ID!): Boolean
        createPage(page: IPage!): Page
        updatePage(_id: ID!, page: IPage!): Page
    }
`;