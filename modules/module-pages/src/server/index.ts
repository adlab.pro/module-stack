import { feature } from '@module-stack/core';
import { IGraphqlFeature } from '@module-stack/core-server';

import { schema } from './graphql';
import { PageService } from './services';

export const PageFeature = feature<IGraphqlFeature>({
    schema,
    context: {
        PageService: new PageService(),
     }
});
