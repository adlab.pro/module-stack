import { Schema, Document, model , Model} from 'mongoose';
import * as Timestamp from 'mongoose-timestamp';

import { IPage } from '../interfaces';

export interface IPageDocument extends IPage, Document {}

const PageSchema = new Schema({
    url: { type: String, required: true, },
    title: { type: String, required: true, },
    content: { type: String, required: true, },
    published: { type: String, required: true, default: true },
});

PageSchema.plugin(Timestamp);

export const Page = model<IPageDocument>('Page', PageSchema);
