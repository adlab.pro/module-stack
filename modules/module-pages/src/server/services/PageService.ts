import { Page, IPageDocument } from '../models';
import { IPageService, IPage } from '../interfaces';

export class PageService implements IPageService {
    get(_id: any) {
        return Page.findOne({ _id }).exec();
    }

    remove(_id: any) {
        return Page.remove({ _id }).exec();
    }

    fing(query: any) {
        return Page.find(query).exec();
    }

    create(data: IPage) {
        return Page.create(data);
    }

    async update(_id: String, data: IPage) {
        const result = await Page.update({ _id }, data);
        return this.get(_id);
    }
}
