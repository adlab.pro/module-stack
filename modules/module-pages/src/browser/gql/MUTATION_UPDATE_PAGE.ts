import gql from 'graphql-tag';

export const MUTATION_UPDATE_PAGE = gql`
    mutation editPage($_id: ID!, $page: IPage!) {
        updatePage(_id: $_id, page: $page) {
            _id
            title
            content
       }
    }
`;