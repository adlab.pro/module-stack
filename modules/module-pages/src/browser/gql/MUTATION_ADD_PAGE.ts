import gql from 'graphql-tag';

export const MUTATION_ADD_PAGE = gql`
    mutation addPage($page: IPage!) {
        createPage(page: $page) {
            _id
            title
            content
       }
    }
`;