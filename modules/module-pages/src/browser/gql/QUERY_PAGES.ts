import gql from 'graphql-tag';

export const QUERY_PAGES = gql`
    query pages {
        getPages {
            _id
            url
            title
            content
        }
    }
`;