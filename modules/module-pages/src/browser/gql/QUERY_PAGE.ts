import gql from 'graphql-tag';

export const QUERY_PAGE = gql`
    query page($_id: ID!) {
        getPage(_id: $_id) {
            _id
            url
            title
            content
        }
    }
`;