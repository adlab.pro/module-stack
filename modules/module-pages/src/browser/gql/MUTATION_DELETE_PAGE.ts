import gql from 'graphql-tag';

export const MUTATION_DELETE_PAGE = gql`
    mutation deletePage($_id: ID!) {
        removePage(_id: $_id)
    }
`;