import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { QUERY_PAGE } from '../gql';

export interface IPageProps {
    _id: string;
    render(...props: any[]): any;
}

export function Page(props: IPageProps) {
    return (
        <React.Fragment>
            <Query variables={{ _id: props._id }} query={QUERY_PAGE}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getPage'), loading: data.loading, ...result })}
            </Query>
        </React.Fragment>
    );
}
