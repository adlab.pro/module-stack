import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { QUERY_PAGES } from '../gql';

export interface IPagesProps {
    query: any;
    render(...props: any[]): any;
}

export function Pages(props: IPagesProps) {
    return (
        <React.Fragment>
            <Query query={QUERY_PAGES}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getPages'), loading: data.loading, ...result })}
            </Query>
        </React.Fragment>
    );
}
