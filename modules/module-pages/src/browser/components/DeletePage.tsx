import _ from 'lodash';
import * as React from 'react';
import { Mutation } from 'react-apollo';

import { MUTATION_DELETE_PAGE } from '../gql';

export interface IDeletePageProps {
    render(...props: any[]): any;
}

export function DeletePage(props: IDeletePageProps) {    
    return (
        <React.Fragment>
            <Mutation mutation={MUTATION_DELETE_PAGE}>{(...args) => props.render(...args)}</Mutation>
        </React.Fragment>
    );
}
