import gql from 'graphql-tag';

export const QUERY_CATEGORIES = gql`
    query categories($target: String!) {
        getCategories(target: $target) {
            _id
            title
            target
        }
    }
`;