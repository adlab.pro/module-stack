import gql from 'graphql-tag';

export const MUTATION_DELETE = gql`
    mutation deleteCategory($_id: ID!) {
        removeCategory(_id: $_id)
    }
`;