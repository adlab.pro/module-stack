import gql from 'graphql-tag';

export const MUTATION_ADD = gql`
    mutation addCategory($input: ICategory!) {
        createCategory(input: $input) {
            _id
            title
            target
       }
    }
`;