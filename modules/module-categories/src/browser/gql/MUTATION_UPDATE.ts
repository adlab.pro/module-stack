import gql from 'graphql-tag';

export const MUTATION_UPDATE = gql`
    mutation editCategory($_id: ID!, $input: ICategory!) {
        updateCategory(_id: $_id, input: $input) {
            _id
            title
            target
       }
    }
`;