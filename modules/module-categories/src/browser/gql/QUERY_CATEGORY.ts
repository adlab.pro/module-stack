import gql from 'graphql-tag';

export const QUERY_CATEGORY = gql`
    query category($_id: ID!) {
        getCategory(_id: $_id) {
            _id
            title
            target
        }
    }
`;