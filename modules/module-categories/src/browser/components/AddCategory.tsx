import _ from 'lodash';
import * as React from 'react';
import { Mutation } from 'react-apollo';

import { MUTATION_ADD } from '../gql';

export interface IAddCategoryProps {
    render(...props: any[]): any;
}

export function AddCategory(props: IAddCategoryProps) {    
    return (
        <React.Fragment>
            <Mutation mutation={MUTATION_ADD}>{(...args) => props.render(...args)}</Mutation>
        </React.Fragment>
    );
}
