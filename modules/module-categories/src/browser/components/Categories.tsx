import * as _ from 'lodash';
import * as React from 'react';
import { Query } from 'react-apollo';
import { IRenderPropsComponent } from '@module-stack/core-client';

import { QUERY_CATEGORIES } from '../gql';

export interface ICategoriesProps {
    target: any;
    render(...props: any[]): any;
}

export function Categories(props: ICategoriesProps) {
    return (
        <React.Fragment>
            <Query variables={{ target: props.target }} query={QUERY_CATEGORIES}>
                {({ data, ...result }) => props.render({ data: _.get(data, 'getCategories'), loading: data.loading, ...result })}
            </Query>
        </React.Fragment>
    );
}
