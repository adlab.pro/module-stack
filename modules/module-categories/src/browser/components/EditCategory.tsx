import _ from 'lodash';
import * as React from 'react';
import { Mutation } from 'react-apollo';

import { MUTATION_UPDATE } from '../gql';

export interface IEditItemsProps {
    render(...props: any[]): any;
}

export function EditCategory(props: IEditItemsProps) {
    return (
        <React.Fragment>
            <Mutation mutation={MUTATION_UPDATE}>{(...args) => props.render(...args)}</Mutation>
        </React.Fragment>
    );
}
