import { Category, ICategoryDocument } from '../models';
import { ICategory, ICategoryService } from '../interfaces';

export class CategoryService implements ICategoryService {
    get(_id: any) {
        return Category.findOne({ _id }).exec();
    }

    remove(_id: any) {
        return Category.remove({ _id }).exec();
    }

    fing(query: any) {
        return Category.find(query).exec();
    }

    create(data: ICategory) {
        return Category.create(data);
    }

    async update(_id: String, data: ICategory) {
        const updated = await Category.update({ _id }, data);
        return this.get(_id);
    }
}
