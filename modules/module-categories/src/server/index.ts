import { feature } from '@module-stack/core';
import { IGraphqlFeature } from '@module-stack/core-server';

import { schema } from './graphql';
import { CategoryService } from './services';

export const CategoryFeature = feature<IGraphqlFeature>({
    schema,
    context: {
        CategoryService: new CategoryService(),
     }
});
