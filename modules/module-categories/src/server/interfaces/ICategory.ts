export interface ICategory {
    _id: any;
    title: string;
    target: string;
    updatedAt: string;
    createdAt: string;
    published: Boolean;
    description: string;
}
