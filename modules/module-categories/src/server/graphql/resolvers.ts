export const resolvers = {
    Query: {
        getCategory: (root, { _id }, context) => context.CategoryService.get(_id),
        getCategories: (root, { target }, context) => context.CategoryService.fing({ target }),
    },
    Mutation: {
        removeCategory: (root, { _id }, context) => context.CategoryService.remove(_id),
        createCategory: (root, { input }, context) => context.CategoryService.create(input),
        updateCategory: (root, { _id, input }, context) => context.CategoryService.update(_id, input),
    },
};