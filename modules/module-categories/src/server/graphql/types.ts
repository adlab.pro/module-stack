export const types = `
    type Category {
        _id: ID!
        title: String
        cover: String
        target: String
        updatedAt: String
        createdAt: String
        published: Boolean
        description: String
    }

    input ICategory {
        cover: String
        title: String!
        target: String!
        published: Boolean
        description: String
    }

    type Query {
        getCategory(_id: String): Category
        getCategories(target: String): [Category]
    }

    type Mutation {
        removeCategory(_id: ID!): Boolean
        createCategory(input: ICategory!): Category
        updateCategory(_id: ID!, input: ICategory!): Category
    }
`;