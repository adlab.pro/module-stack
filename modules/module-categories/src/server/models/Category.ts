import * as Timestamp from 'mongoose-timestamp';
import { Schema, Document, model , Model} from 'mongoose';

import { ICategory } from '../interfaces';

export interface ICategoryDocument extends ICategory, Document {}

const PostSchema = new Schema({
    cover: { type: String },
    description: { type: String },
    title: { type: String, required: true },
    target: { type: String, required: true },
    deleted: { type: Boolean, required: true, default: false },
    published: { type: Boolean, required: true, default: true },
});

PostSchema.plugin(Timestamp);

export const Category = model<ICategoryDocument>('Category', PostSchema);
