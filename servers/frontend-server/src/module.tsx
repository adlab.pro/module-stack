import * as React from 'react';
import { Container } from 'inversify';
import { feature, composed } from '@module-stack/core';
import { IReactFeature, SchemaReactFeature } from '@module-stack/core-client';

const base = feature<IReactFeature>({ container: new Container, context: { APOLLO_URI: 'https://locahost:8080/graphql' } });
const auth = feature<IReactFeature>({ context: { login: () => ({}) } });
const dashboard = feature<IReactFeature>({
    context: { refresh: () => ({}) },
    reducers: {
        "@module-stack/__init__": (state = true) => state,
    },
    routes: [
        { path: '/', action: () => ({ component: <div>Test</div> }) },
    ],
});

// Build application
const application = feature(base, auth, dashboard);

// Composed module for configuration
export const module = composed(application, SchemaReactFeature);
