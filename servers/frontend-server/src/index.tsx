import * as React from 'react';
import RedBoxReact from 'redbox-react';

import { module } from './module';
import { start, render } from './bootstrap';

start(module)
    .then(() => console.log("App is running..."))
    .catch(err => render(<RedBoxReact error={err} />));
