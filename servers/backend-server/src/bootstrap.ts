import * as _ from 'lodash';
import { GraphQLServer, Options as  ServerOptions } from 'graphql-yoga';
import { IGraphqlFeature } from '@module-stack/core-server';

export function start(options: ServerOptions, module: IGraphqlFeature) {
    const params = _.merge(options, module.options);
    const server = new GraphQLServer({ 
        mocks: module.mocks,
        schema: module.schema,
        context: module.context, 
        middlewares: module.middlewares, 
        schemaDirectives: module.schemaDirectives, 
    });

    module.router && server.express.use(module.router);

    return new Promise((resolve, reject) => {
        server.start(params, () => resolve(server));
    });
}
