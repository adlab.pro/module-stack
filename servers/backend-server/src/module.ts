import { Router } from 'express';
import { Container } from 'inversify';
import { makeExecutableSchema } from 'graphql-tools';
import { feature, composed } from '@module-stack/core';
import { IGraphqlFeature, SchemaGraphqlFeature } from '@module-stack/core-server';
import { PostFeature } from '@module-stack/module-blog/lib/server';
import { PageFeature } from '@module-stack/module-pages/lib/server';
import { MenuFeature } from '@module-stack/module-menus/lib/server';
import { CommentFeature } from '@module-stack/module-comments/lib/server';

const typeDefs1 = `
    type Query {
        hello(name: String): String
    }
`;

const typeDefs2 = `
    type Query {
        random: Float
    }
`;

const resolvers1 = {
    Query: {
        hello: (root, { name }) => `Hello, ${name || 'World'}!`, 
    }
};

const resolvers2 = {
    Query: {
        random: () => Math.random(), 
    }
};

const router1 = Router();
router1.get('/__test', (req, res) => res.json({ ok: true }))

const router2 = Router();
router2.get('/__test__', (req, res) => res.json({ ok: true, math: Math.random() }))

const schema1 = makeExecutableSchema({ resolvers: resolvers1, typeDefs: typeDefs1 });
const schema2 = makeExecutableSchema({ resolvers: resolvers2, typeDefs: typeDefs2 });

const base = feature<IGraphqlFeature>({ 
    schema: schema1, 
    router: router1,
    options: { playground: '/playground' }, 
});
const random = feature<IGraphqlFeature>({ schema: schema2, router: router2 });

const validate = async (req) => true;

// Build application
const application = feature(base, random, PageFeature, MenuFeature, CommentFeature, PostFeature);

// Composed module for configuration
export const module = composed(application, SchemaGraphqlFeature);
