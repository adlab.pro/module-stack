import { connect } from 'mongoose';

import { module } from './module';
import { start } from './bootstrap';

connect('mongodb://localhost:27017/feature-pages')
    .then(() => console.log('Connected to db...'))
    .catch(err => console.error('Cannot connect to db: ', err));

start({ port: 9090 }, module)
    .then(() => console.log('Graphql server running...'))
    .catch((err) => console.error('Cannot start graphql servet: ', err));