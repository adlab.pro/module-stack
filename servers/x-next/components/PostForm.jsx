import _ from 'lodash';
import React from 'react';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import { Form, Input, Button, Select } from 'antd';

const { Item } = Form;
const { TextArea } = Input;

const ADD_POST = gql`
    mutation addPost($post: IPost!) {
        createPost(post: $post) {
            _id
            title
            author
            content
       }
    }
`;

function PostForm({ onSuccess, onError, user, form }) {     

    form.getFieldDecorator('published', { initialValue: true })
    form.getFieldDecorator('author', { initialValue: _.get(user, 'user_id', '__guest__') })

    const submit = (addPost, post) => e => {
        e.preventDefault();
        addPost({ variables: { post } })
            .then(onSuccess)
            .catch(onError)
    };

    return (
        <React.Fragment>
            <Mutation mutation={ADD_POST}>
                {(addPost, { loading, error }) => (
                    <Form onSubmit={submit(addPost, form.getFieldsValue())}>
                        <Item label="Title">
                            {form.getFieldDecorator('title', { rules: [{ required: true, message: 'Title is required' }] })(
                                <Input type="text" />
                            )}
                        </Item>
                        <Item label="Content">
                            {form.getFieldDecorator('content', { rules: [{ required: true, message: 'Content is required' }] })(
                                <TextArea />
                            )}
                        </Item>
                        <Item label="Tags">
                            {form.getFieldDecorator('tags', { rules: [{ required: true, message: 'Content is required' }] })(
                                <Select mode="tags" />
                            )}
                        </Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </Form>
                )}
            </Mutation>
        </React.Fragment>
    );
}

export default Form.create()(PostForm); 
