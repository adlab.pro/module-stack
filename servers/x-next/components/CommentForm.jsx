import _ from 'lodash';
import React from 'react';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import { Form, Input, Button } from 'antd';

const { Item } = Form;
const { TextArea } = Input;

const ADD_COMMENT = gql`
    mutation addComment($comment: IComment!) {
        createComment(comment: $comment) {
            _id
            ref
            text
            target
            author
       }
    }
`;

function CommentForm({ onSuccess, onError, user, form, targetId, target }) {     
    
    form.getFieldDecorator('ref', { initialValue: targetId })
    form.getFieldDecorator('target', { initialValue: target })
    form.getFieldDecorator('author', { initialValue: _.get(user, 'user_id', '__guest__') })

    const submit = (addComment, comment) => e => {
        e.preventDefault();
        addComment({ variables: { comment } })
            .then(onSuccess)
            .catch(onError)
    };

    return (
        <React.Fragment>
            <Mutation mutation={ADD_COMMENT}>
                {(addComment, { loading, error }) => (
                    <Form onSubmit={submit(addComment, form.getFieldsValue())}>
                        <Item label="Comment">
                            {form.getFieldDecorator('text', { rules: [{ required: true, message: 'Comment text is required' }] })(
                                <TextArea />
                            )}
                        </Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </Form>
                )}
            </Mutation>
        </React.Fragment>
    );
}

export default Form.create()(CommentForm); 
