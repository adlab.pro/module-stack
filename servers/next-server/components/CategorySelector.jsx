import _ from 'lodash';
import { Form, Input, Select } from 'antd';
import React, { Component, Fragment } from 'react';
import { Categories as HOCCategories, AddCategory as HOCAddCategory } from '@module-stack/module-categories/lib/browser';

const { Option } = Select;

export default ({ target, ...props }) => (
    <HOCCategories 
        target={target}
        render={({ data, loading, refetch }) => (
            <Select {...props}>
                {_.map(data, category => <Option value={category._id}>{category.title}</Option>)}
            </Select>
        )}
    />
);
