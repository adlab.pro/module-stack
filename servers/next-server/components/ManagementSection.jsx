import _ from 'lodash';
import React from 'react';
import { Breadcrumb, Col, Row } from 'antd';

const itemRender = (route, params, routes, paths) => {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? <span>{route.breadcrumbName}</span> : <a href={route.path}>{route.breadcrumbName}</a>;
  }

const ManagementSectionComponent = (props) => (
    <div style={{ width: '960px', margin: '0 auto' }}>
        <Row gutter={16} style={{ margin: '20px 0' }}>
            <Col md={18}>
                <Breadcrumb itemRender={itemRender} routes={props.breadcrumbs} />
            </Col>
            <Col md={6} className="text-right">{props.button}</Col>
        </Row>
        {props.children}
    </div>
);

export default ManagementSectionComponent;
