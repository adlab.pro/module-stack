import _ from 'lodash';
import React from 'react';
import { Layout as AntdLayout, Breadcrumb, Row, Col } from 'antd';

import Layout from './Layout';
import { Link } from '../routes';

const { Sider, Content } = AntdLayout;

const itemRender = (route, params, routes, paths) => {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? <span>{route.breadcrumbName}</span> : <Link to={route.path}>{route.breadcrumbName}</Link>;
}

export default (props) => (
    <Layout>
        <div style={{ margin: '15px auto', width: '960px' }}>
            {props.breadcrumbs ? (
                <Row gutter={16} style={{ margin: '20px 0' }}>
                    <Col md={18}>
                        <Breadcrumb itemRender={itemRender} routes={props.breadcrumbs} />
                    </Col>
                    {props.button ? <Col md={6} className="text-right">{props.button}</Col> : null}
                </Row>
            ) : null}
        </div>
       <AntdLayout style={{ height: "calc(100vh - 194px)", margin: '0 auto', width: '960px', backgroundColor: '#fff' }}>
           {props.sider ? <Sider>{props.sider}</Sider> : null}
           <Content style={{ padding: '15px' }}>
                {props.children}
           </Content>
       </AntdLayout>
    </Layout>
);
