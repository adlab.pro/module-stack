import _ from 'lodash';
import React from 'react';
import { Menu, Layout as AntdLayout, Breadcrumb } from 'antd';
import { Menu as HOCMenu } from '@module-stack/module-menus/lib/browser';

import { Link } from '../routes';

const { SubMenu, Item } = Menu;
const { Content, Header, Footer } = AntdLayout;

const item = (page) => (
    _.isEmpty(page.children) 
            ? <Item key={page.href}>
                <Link route={page.href}>
                    <a>{page.title}</a>   
                </Link>
            </Item> 
            : (
                <SubMenu key={page.href} title={page.title}>
                    {_.map(page.children, page => item(page))}
                </SubMenu>
            )
)

const Layout = (props) => (
    <AntdLayout style={{ height: '100vh' }}>
        <Header>
            <HOCMenu 
                _id="5b1f8ffc80e28d016ac5fbe9" 
                render={({ loading, data, error }) => (
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        style={{ lineHeight: '64px' }}
                    >
                        {_.map(_.get(data, 'elements'), page => item(page))}
                    </Menu> 
                )}
            />
        </Header> 
        <Content>
            {props.children}
        </Content>
        <Footer style={{ textAlign: 'center' }}>
                &copy; CMS Stack
        </Footer> 
    </AntdLayout> 
);

export default Layout;
