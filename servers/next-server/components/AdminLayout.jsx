import _ from 'lodash';
import React from 'react';
import qs from 'query-string';
import { Menu as HOCMenu } from '@module-stack/module-menus/lib/browser';
import { Menu, Layout as AntdLayout, Breadcrumb, Card, Col, Row } from 'antd';

import { ModuleContext } from '../modules/admin';
import ApplicationRouter from '../utils/ApplicationRouter';

const { SubMenu, Item } = Menu;
const { Content, Header, Footer } = AntdLayout;

const item = (page) => (
    _.isEmpty(page.children) 
            ? (
                <Item key={page.href}>
                    <a href={page.href}>{page.title}</a>
                </Item>
            ) 
            : (
                <SubMenu key={page.href} title={page.title}>
                    {_.map(page.children, page => item(page))}
                </SubMenu>
            )
)

const itemRender = (route, params, routes, paths) => {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? <span>{route.breadcrumbName}</span> : <a href={route.path}>{route.breadcrumbName}</a>;
  }

const Layout = (props) => (
    <ModuleContext.Consumer>
        {module => (
            <AntdLayout style={{ height: '100vh' }}>
                <Header>
                <Menu
                    theme="dark"
                    mode="horizontal"
                    style={{ lineHeight: '64px' }}
                >
                    {_.filter(module.links, { scope: 'management' }).map(link => item(link))}
                </Menu>
                </Header> 
                <Content style={{ backgroundColor: '#fff' }}>
                    <div style={{ width: '960px', margin: '0 auto' }}>
                        {process.browser
                            ? (<ApplicationRouter params={qs.parse(location.toString())} basepath="/admin" module={module} router={props.router} />) 
                            : null}
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                        &copy; Test Modular Blog
                </Footer> 
            </AntdLayout> 
        )}
    </ModuleContext.Consumer> 
);

export default Layout;
