import _ from 'lodash';
import React from 'react';
import qs from 'query-string';
import ReactMarkdown from 'react-markdown';
import { Layout as AntdLayout, Menu, Card, Pagination, Row, Col, Icon } from 'antd';
import { Categories as HOCCategories } from '@module-stack/module-categories/lib/browser';
import { Items as HOCItems, Item as HOCItem } from '@module-stack/module-shop/lib/browser';

import { Link, Router } from '../routes';
import ContentPage from '../components/ContentPage';

const { Item, SubMenu } = Menu;
const { Sider, Content } = AntdLayout;

function View(props) {
    return (
        <HOCItem 
            _id={props.item}
            render={({ data, loading }) => (
                !loading ? (
                    <div>
                        <h1>{data.name}</h1>
                        <article>
                            <ReactMarkdown source={data.description} />
                        </article>
                    </div>
                ) : null
            )}
        />
    );
}

function Catalog(props) {
    return (
        <HOCItems
            query={props.router.query} 
            render={({ data = {}, loading }) => {
                const { page, total, limit, pages, docs } = data;
                
                const grid = _.chunk(docs, 3);

                return (
                    <div>
                        {_.map(grid, (row, rowIndex) => (
                            <Row gutter={12} key={rowIndex}>
                                {_.map(row, (item, itemIndex) => (
                                    <Col md={8} key={itemIndex}>
                                        <Card
                                            cover={item.image ? <img src={item.image} /> : <Icon style={{ fontSize: '100px' }} type="shopping-cart" /> }
                                        >
                                            <Card.Meta 
                                                title={<Link to={`/shop?${qs.stringify({ ...props.router.query, item: item._id })}`}>{item.name}</Link>}
                                                description={item.shortcut}
                                            />
                                        </Card>
                                    </Col>
                                ))}
                            </Row> 
                        ))}
                        <br/>
                        <Row>
                            <Col>
                                <Pagination 
                                    total={total} 
                                    pageSize={limit}
                                    showSizeChanger 
                                    defaultCurrent={page} 
                                    onChange={(page, limit) => Router.pushRoute(`/shop?${qs.stringify({ ...props.router.query, page, limit })}`)} 
                                    onShowSizeChange={(current, limit) => Router.pushRoute(`/shop?${qs.stringify({ ...props.router.query, limit })}`)} 
                                />
                            </Col>
                        </Row>
                    </div>
                )
            }}
        />
    );
}

export default (props) => (
    <ContentPage
        breadcrumbs={[
            {
                path: '/',
                breadcrumbName: 'CMS Stack'
            }, {
                path: '/shop',
                breadcrumbName: 'Catalog'
            },
        ]}
        sider={
            <HOCCategories
                target="__items__" 
                render={({ data, loading }) => (
                    <Menu
                        mode="inline"
                        style={{ height: '100%' }}
                        selectedKeys={[ props.router.query.category || 'all' ]}
                        onSelect={({ key }) => Router.pushRoute(key === 'all' ? '/shop' : `/shop?category=${key}`)}
                    > 
                        <Item key={'all'}>All</Item>
                        {_.map(data, category => <Item key={category._id}>{category.title}</Item>)}
                    </Menu>
                )}
            />
        }
    >
         {_.isEmpty(props.router.query.item) ? <Catalog {...props} /> : <View {...props} item={props.router.query.item} />}
    </ContentPage>
);
