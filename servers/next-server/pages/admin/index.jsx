import _ from 'lodash';
import React from 'react';

import { Router } from '../../routes';
import AuthService from '../../utils/AuthService';
import AdminLayout from '../../components/AdminLayout';


export default class AdminIndex extends React.Component {
    componentDidMount() {
        const { auth } = this.props;
        const { profile } = auth;

        if (!auth.loggedIn || _.get(profile, 'app_metadata.role') !== 'admin') {
            Router.pushRoute('/')
        }
    }

    render() {
        const { auth: { loggedIn, profile } } = this.props;

        return (
            loggedIn && _.get(profile, 'app_metadata.role') === 'admin' ? (
                <AdminLayout {...this.props} />
            ) : <h1>403</h1>
        );
    }
}
