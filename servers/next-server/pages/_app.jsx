import _ from 'lodash'
import React from 'react'
import Head from 'next/head'
import PropTypes from 'prop-types'
import App, { Container } from 'next/app'
import { ApolloProvider } from 'react-apollo'
import { Profile } from '@module-stack/module-auth0/lib/browser'

import AuthService from '../utils/AuthService'
import withApolloClient from '../utils/withApollo'
import NextModule, { ModuleContext } from '../modules/admin'    

class MyApp extends App {
    state = { loggedIn: false };

    async componentDidMount() {
        this.auth = new AuthService();

        this.auth.callback = () => {
            this.setState({ loggedIn: this.auth.loggedIn() });
        };
    }

    login() {
        this.auth.login();
    }

    logout() {
        this.auth.logout();
    }

    render() {
        const { Component, pageProps, apolloClient, router } = this.props;

        return (
            <ModuleContext.Provider value={NextModule}>
                <Container>
                    <Head>
                        <meta charSet='utf-8' />
                        <meta name='viewport' content='width=device-width, initial-scale=1' />
                        <script src="//cdn.auth0.com/js/auth0/9.0.0/auth0.min.js"></script>
                        <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/antd/3.5.4/antd.min.css' />
                        <style>{`
                            table {
                                border: 1px solid #d3d3d3;
                                border-bottom: 0 !important;
                            }
                            .text-right {
                                text-align: right;
                            }
                        `}</style>
                    </Head>
                    <ApolloProvider client={apolloClient}>
                        <Profile access_token={this.props.token} render={({ loading, data })=> (
                            <Component 
                                {...pageProps}
                                {...this.props} 
                                auth={{ 
                                    profile: data,
                                    token: this.props.token,
                                    loggedIn: !!this.props.token, 
                                    login: this.login.bind(this), 
                                    logout: this.logout.bind(this), 
                                }} 
                            />
                        )} />
                    </ApolloProvider>
                </Container>
            </ModuleContext.Provider>
        )
    }
}

export default withApolloClient(MyApp)