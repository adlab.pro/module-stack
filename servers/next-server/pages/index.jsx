import _ from 'lodash';
import React from 'react';
import { Button } from 'antd';

import { Layout } from '../components';

const Index = ({ auth }) => (
    <Layout style={{ height: '100vh' }}>
        <h1>X-Stack</h1> 
        {!auth.loggedIn ? <Button onClick={() => auth.login()}>Login</Button> : (
            <pre>{JSON.stringify({ user: auth.profile }, null, 2)}</pre>
        )}
    </Layout> 
);

export default Index;
