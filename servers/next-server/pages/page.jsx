import _ from 'lodash';
import React from 'react';
import { Spin } from 'antd';
import ReactMarkdown from 'react-markdown';
import { Page as HOCPage } from '@module-stack/module-pages/lib/browser';

import ContentPage from '../components/ContentPage';

export default (props) => (
    props.router.query._id ? <HOCPage
        _id={props.router.query._id}
        render={({ data, loading }) => (
            <ContentPage
                breadcrumbs={[
                    {
                        path: '/',
                        breadcrumbName: 'CMS Stack'
                    }, {
                        path: `/page/${_.get(data, '_id')}`,
                        breadcrumbName: _.get(data, 'title'),
                    },
                ]}
            >
                {loading ? <Spin /> : (
                    <div>
                        <h1>{_.get(data, 'title')}</h1>
                        <article>
                            <ReactMarkdown source={_.get(data, 'content')} />
                        </article>
                    </div>
                )}
            </ContentPage>
        )}
    /> : null
);
