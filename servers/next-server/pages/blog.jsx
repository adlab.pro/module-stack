import _ from 'lodash';
import React from 'react';
import moment from 'moment';
import qs from 'query-string';
import ReactMarkdown from 'react-markdown';
import { Layout as AntdLayout, Menu, Card, Spin, Divider, Avatar } from 'antd';
import { Comments as HOCCOmments } from '@module-stack/module-comments/lib/browser';
import { Categories as HOCCategories } from '@module-stack/module-categories/lib/browser';
import { Posts as HOCPosts, Post as HOCPost } from '@module-stack/module-blog/lib/browser';

import { Link, Router } from '../routes';
import CommentForm from '../components/CommentForm';
import ContentPage from '../components/ContentPage';

const { Item, SubMenu } = Menu;
const { Sider, Content } = AntdLayout;

const List = props => (
    <HOCPosts
            query={props.router.query} 
            render={({ data, loading }) => (
                <div>
                    {_.map(data, post => (
                        <Card style={{ marginBottom: '15px' }}>
                            <Card.Meta 
                                description={post.shortcut}  
                                title={<Link to={`/blog?${qs.stringify({ ...props.router.query, _id: post._id })}`}>{post.title}</Link>}
                            />
                        </Card>
                    ))}
                </div>   
            )}
        />
);

const Comment = props => (
    <Card
        style={{ marginBottom: '15px' }}
    >
        <Card.Meta
            title={props.comment.text}
            description={moment(props.comment.createdAt).format('LLLL')}
            avatar={<Avatar>CMS Stack</Avatar>}
        />
    </Card>
);

const View = props => (
    props._id ? <HOCPost
            _id={props._id} 
            render={({ data, loading }) => (
                loading ? <Spin /> : (
                    <div>
                        <div>
                            <h1>{data.title}</h1>
                            <article>
                                <ReactMarkdown source={data.content} />
                            </article>
                        </div>
                        <div>   
                            <Divider>Comments</Divider>
                            <HOCCOmments 
                                refId={props._id}
                                target="__posts__"
                                render={({ data, loading, refresh }) => (
                                    <div>
                                        {_.map(data, comment => <Comment comment={comment} />)}
                                        <CommentForm 
                                            onError={(err) => console.log('Cannot add comment', err)} 
                                            onSuccess={() => location.reload()} 
                                            user="__guest__" 
                                            targetId={props._id} 
                                            target="__posts__"
                                        />
                                    </div>
                                )}
                            />
                        </div>
                    </div>
                )
            )}
        /> : null
);

export default (props) => (
    <ContentPage
        breadcrumbs={[
            {
                path: '/',
                breadcrumbName: 'CMS Stack'
            }, {
                path: '/blog',
                breadcrumbName: 'Blog'
            },
        ]}
        sider={
            <HOCCategories
                target="__posts__" 
                render={({ data, loading }) => (
                    <Menu
                        mode="inline"
                        style={{ height: '100%' }}
                        defaultSelectedKeys={[ props.router.query.category || 'all' ]}
                        onSelect={({ key }) => Router.pushRoute(key === 'all' ? '/blog' : `/blog?${qs.stringify({ category: key })}`)}
                    > 
                        <Item key={'all'}>All</Item>
                        {_.map(data, category => <Item key={category._id}>{category.title}</Item>)}
                    </Menu>
                )}
            />
        }
    >
       {props.router.query._id ? <View {...props} _id={props.router.query._id} /> : <List {...props} />} 
    </ContentPage>
);
