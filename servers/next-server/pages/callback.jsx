import React from 'react'
import { Router } from '../routes'
import AuthService from '../utils/AuthService'

export default class extends React.Component {

  componentDidMount() {
    this.auth = new AuthService();
    this.auth.parseHash(() => {
        Router.pushRoute('/');
    });
  }

  render () {
    return <div />;
  }
}