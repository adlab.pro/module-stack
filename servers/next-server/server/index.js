const _ = require('lodash');
const next = require('next');
const { Router } = require('express');
const mongoose = require('mongoose');
const { GraphQLServer } = require('graphql-yoga');
const { makeExecutableSchema } = require('graphql-tools');
const { feature, composed } = require('@module-stack/core');
const { CategoryFeature } = require('@module-stack/module-categories/lib/server');
const { ShopFeature } = require('@module-stack/module-shop/lib/server');
const { Auth0Feature } = require('@module-stack/module-auth0/lib/server');
const { CommentFeature } = require('@module-stack/module-comments/lib/server');
const { MenuFeature } = require('@module-stack/module-menus/lib/server');
const { PostFeature } = require('@module-stack/module-blog/lib/server');
const { PageFeature } = require('@module-stack/module-pages/lib/server');
const { SchemaGraphqlFeature } = require('@module-stack/core-server');

mongoose.connect('mongodb://localhost:27017/local')
    .then(() => console.log('Connected to db...'))
    .catch(err => console.error('Cannot connect to db: ', err));

const router = require('../routes');

const typeDefs = `
    type Query {
        hello(name: String): String
    }
`;

const resolvers = {
    Query: {
        hello: (root, { name }) => `Hello, ${name || 'World'}!`, 
    }
};

const schema = makeExecutableSchema({ resolvers, typeDefs });

const port = parseInt(process.env.PORT, 10) || 9090;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = router.getRequestHandler(app);

function start(options, module) {
    const params = _.merge(options, module.options);
    const server = new GraphQLServer({ 
        mocks: module.mocks,
        schema: module.schema,
        context: module.context, 
        middlewares: module.middlewares, 
        schemaDirectives: module.schemaDirectives, 
    });

    module.router && server.express.use(module.router);

    return new Promise((resolve, reject) => {
        server.start(params, () => resolve(server));
    });
}

function exclude(paths, handler) {
    return function(req, res, next) { 
        const match = _.find(paths, path => _.includes(req.path, path));
        if (!match) handler(req, res, next);
        else next();
     }
};

// Inject Next.js handler
const NextRouter = Router();
NextRouter.use(exclude(['/service'], handle));

const NextFeature = feature({ 
    schema, 
    router: NextRouter, 
    options: { 
        endpoint: '/service/graphql',
        playground: '/service/playground',
        subscriptions: '/service/subscriptions',
    },
});

const application = feature(NextFeature, PageFeature, MenuFeature, CommentFeature, PostFeature, CategoryFeature, ShopFeature, Auth0Feature({
    domain: "adlab.auth0.com",
    clientId: "hTdXxzAYFwdtLiVtT5sRpigERIcSukih",
    clientSecret: "ZZUs35FGt2Vf48DZmyXCfcJGpVLwZ0235pp9SFHi1pDNecRFdNX-bOtfGHa0BSPb",
}));

const __module = composed(application, SchemaGraphqlFeature)

app
    .prepare()
    .then(() => {
        start({ port }, __module)
            .then(() => console.log('> Next server started'))
            .catch(err => console.error('> Next server can not be started: ', err))
    })
    .catch(err => console.error("Error: ", err))
