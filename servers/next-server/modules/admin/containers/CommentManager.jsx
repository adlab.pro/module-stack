import _ from 'lodash';
import React from 'react';
import { Router } from '../../../routes';
import { Table, Button, Popconfirm, Modal, message, Radio } from 'antd';
import { Comments as HOCComments, DeleteComment as HOCDeleteComment } from '@module-stack/module-comments/lib/browser';

import CategoryForm from '../components/CategoryForm';
import ManagementSection from '../../../components/ManagementSection';

const { Button: RadioButton, Group }= Radio;

export default class Comments extends React.Component {
    columns = [
        {
            title: "Text",
            dataIndex: 'text',
        },
        {
            title: "Actions",
            render: record => (
                <div>
                    <HOCDeleteComment
                        render={(remove) => (
                            <Popconfirm
                                title="Are you sure?" 
                                onConfirm={() => {
                                    remove({ variables: { _id: record._id } })
                                        .then(() => location.reload())
                                        .catch(() => message.error("Server error :( Try later..."));
                                    }
                                }
                            >
                                <Button size="small" type="danger">Delete</Button>
                            </Popconfirm> 
                        )}
                    /> 
                </div>
            ),
        },
    ];

    render() {
        const { router: { query }, module } = this.props;

        return (
            <ManagementSection
                button={(
                    <React.Fragment>
                        <Group defaultValue={_.get(query, 'scope', '__posts__')} size="small" type="primary" onChange={e => Router.pushRoute(`/admin?scope=${e.target.value}#categories`)}>
                            {_.map(module.scope, scope => <RadioButton value={scope._id}>{scope.name}</RadioButton>)}
                        </Group>
                    </React.Fragment>   
                )}
                breadcrumbs={[
                    {
                        path: '/admin',
                        breadcrumbName: 'Admin Panel'
                      }, {
                        path: '/admin#comments',
                        breadcrumbName: 'Comments'
                      },
                ]}
            >
                <HOCComments 
                    target={_.get(query, 'scope', '__posts__')}
                    render={({ data, loading }) => (
                        <div>
                            <Table
                                rowKey="_id"
                                loading={loading}
                                dataSource={data}
                                columns={this.columns}
                            />
                        </div>
                    )}
                /> 
            </ManagementSection>
        );
    }
}
