import _ from 'lodash';
import React from 'react';
import { Table, Button, Popconfirm, Modal, message } from 'antd';
import { Pages as HOCPages, DeletePage as HOCDeletePage } from '@module-stack/module-pages/lib/browser';

import PageForm from '../components/PageForm';
import ManagementSection from '../../../components/ManagementSection';

export default class Items extends React.Component {
    state = {
        item: {},
        modal: false,
    };

    columns = [
        {
            title: "URL",
            dataIndex: 'url',
        },
        {
            title: "Title",
            dataIndex: 'title',
        },
        {
            title: "Actions",
            render: record => (
                <div>
                    <Button onClick={() => this.setState({ item: record, modal: true })} size="small" type="primary">Edit</Button>
                    {' '}
                    <HOCDeletePage
                        render={(remove) => (
                            <Popconfirm
                                title="Are you sure?" 
                                onConfirm={() => {
                                    remove({ variables: { _id: record._id } })
                                        .then(() => location.reload())
                                        .catch(() => message.error("Server error :( Try later..."));
                                    }
                                }
                            >
                                <Button size="small" type="danger">Delete</Button>
                            </Popconfirm> 
                        )}
                    /> 
                </div>
            ),
        },
    ];

    render() {
        return (
            <ManagementSection
                button={(
                    <Button onClick={() => this.setState({ modal: true })} styl={{ margin: 0 }} size="small" type="primary">Add Page</Button>
                )}
                breadcrumbs={[
                    {
                        path: '/admin',
                        breadcrumbName: 'Admin Panel'
                      }, {
                        path: '/admin/pages',
                        breadcrumbName: 'Pages'
                      },
                ]}
            >
                <HOCPages 
                    query={{}}
                    render={({ data, loading }) => (
                        <div>
                            <Table
                                rowKey="_id"
                                loading={loading}
                                dataSource={data}
                                columns={this.columns}
                            />
                        </div>
                    )}
                />

                <Modal onCancel={() => this.setState({ item: {}, modal: false })} footer={false} visible={this.state.modal}>
                    <PageForm 
                        item={this.state.item || {}}
                        onSuccess={() => location.reload()}
                        onError={() => message.error("Server error :( Try later...")}
                    />    
                </Modal>   
            </ManagementSection>
        );
    }
}
