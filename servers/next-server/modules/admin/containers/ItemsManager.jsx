import _ from 'lodash';
import React from 'react';
import { Router } from '../../../routes';
import { Table, Button, Popconfirm, Modal, message } from 'antd';
import { Items as HOCItems, DeleteItem as HOCDeleteItem } from '@module-stack/module-shop/lib/browser';

import ShopItemForm from '../components/ShopItemForm';
import { ManagementSection } from '../../../components';

export default class Items extends React.Component {
    state = {
        item: {},
        modal: false,
    };

    columns = [
        {
            title: "Name",
            dataIndex: 'name',
        },
        {
            title: "Price",
            dataIndex: 'price',
        },
        {
            title: "QTY",
            dataIndex: 'qty',
            render: record => record < 0 ? "Unlimitted" : record,
        },
        {
            title: "Actions",
            render: record => (
                <div>
                    <Button onClick={() => this.setState({ item: record, modal: true })} size="small" type="primary">Edit</Button>
                    {' '}
                    <HOCDeleteItem
                        render={(remove) => (
                            <Popconfirm
                                title="Are you sure?" 
                                onConfirm={() => {
                                    remove({ variables: { _id: record._id } })
                                        .then(() => location.reload())
                                        .catch(() => message.error("Server error :( Try later..."));
                                    }
                                }
                            >
                                <Button size="small" type="danger">Delete</Button>
                            </Popconfirm> 
                        )}
                    /> 
                </div>
            ),
        },
    ];

    render() {
        const { router } = this.props;
        const query = _.get(router, 'query', {});

        return (
            <ManagementSection
                button={(
                    <Button onClick={() => this.setState({ modal: true })} styl={{ margin: 0 }} size="small" type="primary">Add Item</Button>
                )}
                breadcrumbs={[
                    {
                        path: '/admin',
                        breadcrumbName: 'Admin Panel'
                      }, {
                        path: '/admin#items',
                        breadcrumbName: 'Items'
                      },
                ]}
            >
                <HOCItems 
                    query={_.pick(query, ['page', 'limit'])}
                    render={({ data, loading }) => (
                        <div>
                            {data ? (
                                <Table
                                rowKey="_id"
                                loading={loading}
                                columns={this.columns}
                                dataSource={_.get(data, 'docs', [])}
                                pagination={{
                                    total: data.total,
                                    current: data.page,
                                    pageSize: data.limit,
                                    showSizeChanger: true,
                                    onChange: (page, limit) => Router.pushRoute(`/admin?page=${page || 1}&limit=${limit || 15}/#items`),
                                    onShowSizeChange: (current, limit) => Router.pushRoute(`/admin?page=${query.page || 1}&limit=${limit || 15}/#items`),
                                    }}
                                />
                            ) : null}
                        </div>
                    )}
                />

                <Modal onCancel={() => this.setState({ item: {}, modal: false })} footer={false} visible={this.state.modal}>
                    <ShopItemForm 
                        item={this.state.item || {}}
                        onSuccess={() => location.reload()}
                        onError={() => message.error("Server error :( Try later...")}
                    />    
                </Modal>   
            </ManagementSection>
        );
    }
}
