import _ from 'lodash';
import React from 'react';
import { Table, Button, Popconfirm, Modal, message } from 'antd';
import { Posts as HOCIPosts, DeletePost as HOCDeletePost } from '@module-stack/module-blog/lib/browser';

import PostForm from '../components/PostForm';
import ManagementSection from '../../../components/ManagementSection';

export default class Items extends React.Component {
    state = {
        item: {},
        modal: false,
    };

    columns = [
        {
            title: "Title",
            dataIndex: 'title',
        },
        {
            title: "Actions",
            render: record => (
                <div>
                    <Button onClick={() => this.setState({ item: record, modal: true })} size="small" type="primary">Edit</Button>
                    {' '}
                    <HOCDeletePost
                        render={(remove) => (
                            <Popconfirm
                                title="Are you sure?" 
                                onConfirm={() => {
                                    remove({ variables: { _id: record._id } })
                                        .then(() => location.reload())
                                        .catch(() => message.error("Server error :( Try later..."));
                                    }
                                }
                            >
                                <Button size="small" type="danger">Delete</Button>
                            </Popconfirm> 
                        )}
                    /> 
                </div>
            ),
        },
    ];

    render() {
        return (
            <ManagementSection
                button={(
                    <Button onClick={() => this.setState({ modal: true })} styl={{ margin: 0 }} size="small" type="primary">Add Item</Button>
                )}
                breadcrumbs={[
                    {
                        path: '/admin',
                        breadcrumbName: 'Admin Panel'
                      }, {
                        path: '/admin/posts',
                        breadcrumbName: 'Posts'
                      },
                ]}
            >
                <HOCIPosts 
                    query={{}}
                    render={({ data, loading }) => (
                        <div>
                            <Table
                                rowKey="_id"
                                loading={loading}
                                dataSource={data}
                                columns={this.columns}
                            />
                        </div>
                    )}
                />

                <Modal onCancel={() => this.setState({ item: {}, modal: false })} footer={false} visible={this.state.modal}>
                    <PostForm 
                        item={this.state.item || {}}
                        onSuccess={() => location.reload()}
                        onError={() => message.error("Server error :( Try later...")}
                    />    
                </Modal>   
            </ManagementSection>
        );
    }
}
