import _ from 'lodash';
import React from 'react';
import { Router } from '../../../routes';
import { Table, Button, Popconfirm, Modal, message, Radio } from 'antd';
import { Categories as HOCCategories, DeleteCategory as HOCDeleteCategory } from '@module-stack/module-categories/lib/browser';

import CategoryForm from '../components/CategoryForm';
import ManagementSection from '../../../components/ManagementSection';

const { Button: RadioButton, Group }= Radio;

export default class Items extends React.Component {
    state = {
        item: {},
        modal: false,
    };

    columns = [
        {
            title: "Title",
            dataIndex: 'title',
        },
        {
            title: "Actions",
            render: record => (
                <div>
                    <Button onClick={() => this.setState({ item: record, modal: true })} size="small" type="primary">Edit</Button>
                    {' '}
                    <HOCDeleteCategory
                        render={(remove) => (
                            <Popconfirm
                                title="Are you sure?" 
                                onConfirm={() => {
                                    remove({ variables: { _id: record._id } })
                                        .then(() => location.reload())
                                        .catch(() => message.error("Server error :( Try later..."));
                                    }
                                }
                            >
                                <Button size="small" type="danger">Delete</Button>
                            </Popconfirm> 
                        )}
                    /> 
                </div>
            ),
        },
    ];

    render() {
        const { router: { query }, module } = this.props;

        return (
            <ManagementSection
                button={(
                    <React.Fragment>
                        <Group defaultValue={_.get(query, 'scope', '__posts__')} size="small" type="primary" onChange={e => Router.pushRoute(`/admin?scope=${e.target.value}#categories`)}>
                            {_.map(module.scope, scope => <RadioButton value={scope._id}>{scope.name}</RadioButton>)}
                        </Group>
                        {' | '}
                        <Button onClick={() => this.setState({ modal: true })} styl={{ margin: 0 }} size="small" type="primary">Add Category</Button>
                    </React.Fragment>   
                )}
                breadcrumbs={[
                    {
                        path: '/admin',
                        breadcrumbName: 'Admin Panel'
                      }, {
                        path: '/admin#categories',
                        breadcrumbName: 'Categories'
                      },
                ]}
            >
                <HOCCategories 
                    target={_.get(query, 'scope', '__posts__')}
                    render={({ data, loading }) => (
                        <div>
                            <Table
                                rowKey="_id"
                                loading={loading}
                                dataSource={data}
                                columns={this.columns}
                            />
                        </div>
                    )}
                />

                <Modal onCancel={() => this.setState({ item: {}, modal: false })} footer={false} visible={this.state.modal}>
                    <CategoryForm 
                        item={this.state.item || {}}
                        onSuccess={() => location.reload()}
                        target={_.get(query, 'scope', '__posts__')}
                        onError={() => message.error("Server error :( Try later...")}
                    />    
                </Modal>   
            </ManagementSection>
        );
    }
}
