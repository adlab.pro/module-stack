import _ from 'lodash';
import React from 'react';
import { Row, Col, Card, Button, Divider } from 'antd';

import { ManagementSection } from '../../../components';

const { Meta } = Card;

const styles = {
    cover: {
        padding: '15px 0',
        textAlign: 'center',
    },
    row: {
        marginBottom: '16px',
    },
};

export default class AdminDashboard extends React.Component {
    static card(item) {
        return (
            <Card
                hoverable
                cover={(
                    <div style={styles.cover}>
                        {item.icon}
                    </div>
                )}
            >
                <Meta
                    description={item.description}
                    title={<a href={item.href}>{item.title}</a>}
                />
            </Card>
        );
    }

    render() {
        const { module } = this.props;

        const grid = _.chunk(module.tile, 3);

        return (
            <ManagementSection
                button={(
                    <Button icon="question-circle-o" style={{ margin: 0 }} size="small" type="primary">Help</Button>
                )}
                breadcrumbs={[
                    {
                        path: '/admin',
                        breadcrumbName: 'Admin Panel'
                      }
                ]}
            >
                {_.map(grid, (items, rowIndex) => (
                    <Row style={styles.row} key={rowIndex} gutter={16}>
                        {_.map(items, (item, itemIndex) => (
                            <Col key={itemIndex} md={8}>
                                {item.type === 'card' ? AdminDashboard.card(item) : item}
                            </Col>
                        ))}
                    </Row>
                ))}        
            </ManagementSection>
        );
    }
}
