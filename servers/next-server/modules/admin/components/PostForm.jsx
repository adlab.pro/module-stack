import _ from 'lodash';
import React from 'react';
import { Form, Input, Button, Select, InputNumber } from 'antd';
import { AddPost, EditPost } from '@module-stack/module-blog/lib/browser';

import CategorySelector from '../../../components/CategorySelector';

const { Item } = Form;
const { TextArea } = Input;

function PostForm({ item = {}, onSuccess, onError, user, form }) {     

    form.getFieldDecorator('_id', { initialValue: _.get(item, '_id') });
    form.getFieldDecorator('published', { initialValue: _.get(item, 'published', true) });
    form.getFieldDecorator('author', { initialValue: _.get(user, 'user_id', '__guest__') });

    const submit = (mutation, post) => e => {
        e.preventDefault();
        form.validateFields((err) => {
            if (err) return;

            let variables = { post };
            if (post._id) variables._id = post._id;

            mutation({ variables })
                .then(onSuccess)
                .catch(onError);
        })
    };

    const content = (mutation, { loading, error }) => (
        <Form onSubmit={submit(mutation, form.getFieldsValue())}>
            <Item label="Title">
                {form.getFieldDecorator('title', { rules: [{ required: true, message: 'Title is required' }], initialValue: item.title })(
                    <Input type="text" />
                )}
            </Item>
            <Item label="Shortcut">
                {form.getFieldDecorator('shortcut', { rules: [{ required: true, message: 'Shortcut is required' }], initialValue: item.shortcut })(
                    <TextArea />
                )}
            </Item>
            <Item label="Content">
                {form.getFieldDecorator('content', { rules: [{ required: true, message: 'Content is required' }], initialValue: item.content })(
                    <TextArea />
                )}
            </Item>
            <Item label="Category">
                {form.getFieldDecorator('category', { initialValue: item.category })(
                    <CategorySelector target={'__posts__'} />
                )}
            </Item>
            <Item label="Tags">
                {form.getFieldDecorator('tags', { initialValue: item.tags })(
                    <Select mode="tags" />
                )}
            </Item>
            <Button type="primary" htmlType="submit">Submit</Button>
        </Form>
    );

    return (
        <React.Fragment>
            {!item._id 
                ? <AddPost render={content} />
                : <EditPost render={content} />}
        </React.Fragment>
    );
}

export default Form.create()(PostForm); 
