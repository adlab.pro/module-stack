import _ from 'lodash';
import React from 'react';
import { Form, Input, Button, Select, InputNumber } from 'antd';
import { AddItem, EditItem } from '@module-stack/module-shop/lib/browser';

import CategorySelector from '../../../components/CategorySelector';

const { Item } = Form;
const { TextArea } = Input;

function ShopItemForm({ item = {}, onSuccess, onError, user, form }) {     

    form.getFieldDecorator('_id', { initialValue: _.get(item, '_id') })
    form.getFieldDecorator('published', { initialValue: _.get(item, 'published', true) })
    form.getFieldDecorator('author', { initialValue: _.get(user, 'user_id', '__guest__') })

    const submit = (mutation, input) => e => {
        e.preventDefault();
        form.validateFields((err) => {
            if (err) return;

            let variables = { input };
            if (input._id) variables._id = input._id;

            mutation({ variables })
                .then(onSuccess)
                .catch(onError);
        })
    };

    const content = (mutation, { loading, error }) => (
        <Form onSubmit={submit(mutation, form.getFieldsValue())}>
            <Item label="Title">
                {form.getFieldDecorator('name', { rules: [{ required: true, message: 'Title is required' }], initialValue: item.name })(
                    <Input type="text" />
                )}
            </Item>
            <Item label="Price">
                {form.getFieldDecorator('price', { rules: [{ required: true, message: 'Price is required' }], initialValue: item.price })(
                    <InputNumber />
                )}
            </Item>
            <Item label="Shortcut">
                {form.getFieldDecorator('shortcut', { rules: [{ required: true, message: 'Content is required' }], initialValue: item.shortcut })(
                    <TextArea />
                )}
            </Item>
            <Item label="Content">
                {form.getFieldDecorator('description', { rules: [{ required: true, message: 'Content is required' }], initialValue: item.description })(
                    <TextArea />
                )}
            </Item>
            <Item label="Category">
                {form.getFieldDecorator('category', { initialValue: item.category })(
                    <CategorySelector target={'__items__'} />
                )}
            </Item>
            <Item label="Tags">
                {form.getFieldDecorator('tags', { initialValue: item.tags })(
                    <Select mode="tags" />
                )}
            </Item>
            <Button type="primary" htmlType="submit">Submit</Button>
        </Form>
    );

    return (
        <React.Fragment>
            {!item._id 
                ? <AddItem render={content} />
                : <EditItem render={content} />}
        </React.Fragment>
    );
}

export default Form.create()(ShopItemForm); 
