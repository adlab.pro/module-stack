import _ from 'lodash';
import React from 'react';
import { Form, Input, Button, Select, InputNumber } from 'antd';
import { AddCategory, EditCategory } from '@module-stack/module-categories/lib/browser';

const { Item } = Form;
const { TextArea } = Input;

function PostForm({ item = {}, onSuccess, onError, user, form, target }) {     

    form.getFieldDecorator('_id', { initialValue: _.get(item, '_id') });
    form.getFieldDecorator('published', { initialValue: _.get(item, 'published', true) });
    form.getFieldDecorator('target', { initialValue: _.get(item, 'target', target) || target });

    const submit = (mutation, input) => e => {
        e.preventDefault();
        form.validateFields((err) => {
            if (err) return;

            let variables = { input };
            if (input._id) variables._id = input._id;

            mutation({ variables })
                .then(onSuccess)
                .catch(onError);
        })
    };

    const content = (mutation, { loading, error }) => (
        <Form onSubmit={submit(mutation, form.getFieldsValue())}>
            <Item label="Title">
                {form.getFieldDecorator('title', { rules: [{ required: true, message: 'Title is required' }], initialValue: item.title })(
                    <Input type="text" />
                )}
            </Item>
            <Item label="Description">
                {form.getFieldDecorator('description', { initialValue: item.content })(
                    <TextArea />
                )}
            </Item>
            <Item label="Caover URL">
                {form.getFieldDecorator('cover', { rules: [{ type: 'url', message: 'URL to cover image' }], initialValue: item.cover })(
                    <Input type="url" />
                )}
            </Item>
            <Button type="primary" htmlType="submit">Submit</Button>
        </Form>
    );

    return (
        <React.Fragment>
            {!item._id 
                ? <AddCategory render={content} />
                : <EditCategory render={content} />}
        </React.Fragment>
    );
}

export default Form.create()(PostForm); 
