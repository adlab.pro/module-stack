import _ from 'lodash';
import React from 'react';
import { Form, Input, Button, Select, InputNumber } from 'antd';
import { AddPage, EditPage } from '@module-stack/module-pages/lib/browser';

const { Item } = Form;
const { TextArea } = Input;

function PageForm({ item = {}, onSuccess, onError, user, form }) {     

    form.getFieldDecorator('_id', { initialValue: _.get(item, '_id') });

    const submit = (mutation, page) => e => {
        e.preventDefault();
        form.validateFields((err) => {
            if (err) return;

            let variables = { page };
            if (page._id) variables._id = page._id;

            mutation({ variables })
                .then(onSuccess)
                .catch(onError);
        })
    };

    const content = (mutation, { loading, error }) => (
        <Form onSubmit={submit(mutation, form.getFieldsValue())}>
            <Item label="Title">
                {form.getFieldDecorator('title', { rules: [{ required: true, message: 'Title is required' }], initialValue: item.title })(
                    <Input type="text" />
                )}
            </Item>
            <Item label="URL">
                {form.getFieldDecorator('url', { rules: [{ required: true, message: 'Page url is required' }], initialValue: item.url })(
                    <Input type="text" />
                )}
            </Item>
            <Item label="Content">
                {form.getFieldDecorator('content', { rules: [{ required: true, message: 'Content is required' }], initialValue: item.content })(
                    <TextArea />
                )}
            </Item>
            <Button type="primary" htmlType="submit">Submit</Button>
        </Form>
    );

    return (
        <React.Fragment>
            {!item._id 
                ? <AddPage render={content} />
                : <EditPage render={content} />}
        </React.Fragment>
    );
}

export default Form.create()(PageForm); 
