import { Icon } from 'antd';
import { createContext } from 'react';
import { Route } from 'react-router-dom';
import { feature, schema, composed } from '@module-stack/core';

import PagesManager from './containers/PagesManager';
import ItemsManager from './containers/ItemsManager';
import PostsManager from './containers/PostsManager';
import AdminDashboard from './containers/AdminDashboard';
import CommentManager from './containers/CommentManager';
import CategoryManager from './containers/CategoryManager';
import { BlogFeatureSchemaAdmin } from '../core/NextBlogSchema';

const NotImplemented = props => <h1>Not implemented...</h1>

const icon = {
    fontSize: '150px'
};

const NextAppFeature = feature({
    scope: [
        { _id: '__posts__', name: 'Posts' },
        { _id: '__items__', name: 'Items' },
    ],
    tile: [
        { type: 'card', icon: <Icon style={icon} type="profile" />, title: 'Pages', href: '/admin#pages', description: 'Manage site pages' },
        { type: 'card', icon: <Icon style={icon} type="file-text" />, title: 'Posts', href: '/admin#posts', description: 'Blog posts management' },
        { type: 'card', icon: <Icon style={icon} type="message" />, title: 'Comments', href: '/admin#comments', description: 'Comments moderation' },
        { type: 'card', icon: <Icon style={icon} type="shopping-cart" />, title: 'Shop Items', href: '/admin#items', description: 'Shop items management' },
        { type: 'card', icon: <Icon style={icon} type="tags" />, title: 'Categories', href: '/admin#categories', description: 'Manage categories for app scopes' },
    ],
    links: [
        { href: '#/', title: 'Admin', scope: 'management' },
        { href: '#/pages', title: 'Pages', scope: 'management' },
        { href: '#/posts', title: 'Posts', scope: 'management' },
        { href: '#/items', title: 'Items', scope: 'management' },
        { href: '#/comments', title: 'Comments', scope: 'management' },
        { href: '#/categories', title: 'Categories', scope: 'management' },
    ],
    routes: [
        { exact: true, path: '/', component: AdminDashboard },
        
        { path: '/pages', component: PagesManager },
        { path: '/posts', component: PostsManager },
        { path: '/items', component: ItemsManager },
        { path: '/comments', component: CommentManager },
        { path: '/categories', component: CategoryManager },
    ],
});


const module = composed(NextAppFeature, BlogFeatureSchemaAdmin);

export const ModuleContext = createContext(module);

export default module;
