import _ from 'lodash';
import { SchemaReactFeature } from '@module-stack/core-client';

export const BlogFeatureSchemaAdmin = _.assign(SchemaReactFeature, { 
    tile: {},
    scope: {},
    links: {},
 });
