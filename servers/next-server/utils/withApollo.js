import Head from 'next/head'
import cookie from 'cookie'
import propTypes from 'prop-types'
import { getDataFromTree } from 'react-apollo'

const initApollo = require('./initApollo');

function parseCookies(req, options = {}) {
  return cookie.parse(
    req ? req.headers.cookie || '' : document.cookie,
    options
  )
}


export default (App) => {
  return class Apollo extends React.Component {
    static displayName = 'withApollo(App)'
    static async getInitialProps (ctx) {
      const { Component, router, ctx: { req, res } } = ctx

      let appProps = {}
      if (App.getInitialProps) {
        appProps = await App.getInitialProps(ctx)
      }

      const apolloState = {}

      // Run all GraphQL queries in the component tree
      // and extract the resulting data
      const apollo = initApollo(undefined, {
        getToken: () => parseCookies(req).accessToken
      })
      try {
        // Run all GraphQL queries
        await getDataFromTree(
          <App
            {...appProps}
            router={router}
            Component={Component}
            apolloClient={apollo}
            apolloState={apolloState}
            token={parseCookies(req).accessToken}
          />
        )
      } catch (error) {
        // Prevent Apollo Client GraphQL errors from crashing SSR.
        // Handle them in components via the data.error prop:
        // http://dev.apollodata.com/react/api-queries.html#graphql-query-data-error
        console.error('Error while running `getDataFromTree`', error)
      }

      if (!process.browser) {
        // getDataFromTree does not call componentWillUnmount
        // head side effect therefore need to be cleared manually
        Head.rewind()
      }

      // Extract query data from the Apollo store
      apolloState.data = apollo.cache.extract()

      return {
        apolloState,
        ...appProps,
        token: parseCookies(req).accessToken,
      }
    }

    constructor (props) {
      super(props)
      // `getDataFromTree` renders the component first, the client is passed off as a property.
      // After that rendering is done using Next's normal rendering pipeline
      this.apolloClient = props.apolloClient || initApollo(props.apolloState.data, {
        getToken: () => parseCookies().accessToken
      })
    }

    render () {
      return <App {...this.props} apolloClient={this.apolloClient} />
    }
  }
}