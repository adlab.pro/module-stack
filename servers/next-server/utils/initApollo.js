require('isomorphic-fetch');
const { ApolloLink } = require('apollo-link');
const { HttpLink } = require('apollo-link-http');
const { ApolloClient } = require('apollo-client');
const { WebSocketLink } = require('apollo-link-ws');
const { setContext } = require('apollo-link-context');
const { InMemoryCache, NormalizedCacheObject } = require('apollo-cache-inmemory');
const { SubscriptionClient } = require('subscriptions-transport-ws');

let apolloClient = null

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  global.fetch = fetch
}

function create(initialState, { getToken }) {
  const httpLink = new HttpLink({ uri: `http://localhost:9090/service/graphql` });
  const authLink = setContext((_, { headers }) => {
    const token = getToken()
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    }
  })

  return new ApolloClient({
      ssrMode: !process.browser,
      link: authLink.concat(httpLink),
      cache: new InMemoryCache().restore(initialState),
  });
}

module.exports = function initApollo(initialState, options) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create(initialState, options)
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState, options)
  }

  return apolloClient
}
