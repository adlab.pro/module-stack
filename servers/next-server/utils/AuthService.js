import React from 'react'
import Cookie from 'cookie-monster'

export default class AuthService {
  constructor(clientId, domain) {
    // Configure Auth0
    this.clientId = clientId;
    this.domain = domain;

    
    this.auth0 = new window.auth0.WebAuth({
      domain: "adlab.auth0.com",
      clientID: "hTdXxzAYFwdtLiVtT5sRpigERIcSukih",
      scope: 'openid profile',
      responseType: 'token id_token',
      redirectUri: 'http://localhost:9090/callback'
    });

    // binds login functions to keep this context
    this.login = this.login.bind(this);
  }

  parseHash(callback) {
    this.auth0.parseHash((err, result) => {
      if(err) {
        console.log(err);
        callback(false);
        this.logout();
        return;
      }

      this.setToken(result.accessToken);
      callback(true);
    });
  }

  async profile() {
    const token = this.getToken();
    return new Promise((resolve, reject) => {
        this.auth0.client.userInfo(token, (err, profile) => {
            if (err) reject(err);
            else resolve(profile);
            return;
        })
    });
  }

  login() {    
    this.auth0.authorize();
  }

  loggedIn(){
    // Checks if there is a saved token and it's still valid
    return !!this.getToken();
  }

  setToken(accessToken){
    // Saves user token to localStorage
    Cookie.setItem('accessToken', accessToken);
  }

  getToken(){
    // Retrieves the user token from localStorage
    return Cookie.getItem('accessToken');
  }

  logout(){
    // Clear user token from localStorage
    Cookie.removeItem('accessToken');
  }
}