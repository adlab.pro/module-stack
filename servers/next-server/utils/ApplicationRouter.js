import _ from 'lodash';
import React from 'react';
import { HashRouter, Route, withRouter } from 'react-router-dom';

export default ({ module, router, params, ...props }) => (
    <HashRouter>
        <div>
            {_.map(module.routes, route => <Route exact={route.exact} path={route.path} render={() => React.createElement(route.component, { ...route, ...props, module,  params, router }, null)} />)}
        </div>
    </HashRouter>  
);
