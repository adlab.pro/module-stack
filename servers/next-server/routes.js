// version 1.2.0

const Routes = require('next-routes');
module.exports = router = Routes();

router.add('test', '/test/:name?');
router.add('page', '/page/:_id?');
router.add('post', '/post/:_id?');
