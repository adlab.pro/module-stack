import * as _ from 'lodash';
import * as Nats from 'nats';
import * as Hemera from 'nats-hemera';
import { IHemeraFeature, IHemeraAction } from '@module-stack/core-hemera';

export function start(url, options, module: IHemeraFeature) {
    const connection = Nats.connect(url);
    const hemera = new Hemera(connection, options);

    return new Promise((resolve) => {
        hemera.ready(() => {
            // Injecting context
            hemera.context$ = _.assign(module.context, { container: module.container });

            // Added actions
            _.each(module.actions, (action: IHemeraAction) => hemera.add(action.pattern).use(action.middleware).end(action.action));

            resolve(hemera);
        });
    });
}
