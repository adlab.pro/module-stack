import {Container} from 'inversify';
import {feature, composed} from '@module-stack/core';
import {IHemeraFeature, SchemaHemeraFeature} from '@module-stack/core-hemera';

const base = feature<IHemeraFeature>({
    container: new Container,
    context: {APOLLO_URI: 'https://locahost:8080/graphql'}
});

const validate = async (req) => true;

const auth = feature<IHemeraFeature>({context: {login: () => ({})}});
const dashboard = feature<IHemeraFeature>({});

// Build application
const application = feature(base, auth, dashboard);

// Composed module for configuration
export const module = composed(application, SchemaHemeraFeature);
