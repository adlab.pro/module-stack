import { module } from './module';
import { start } from './bootstrap';

start('nats://localhost:4222', { logLevel: 'info' }, module)
    .then(hemera => console.log('Hemera server is running!'))
    .catch(err => console.log('Can not start hemera server: ', err));
