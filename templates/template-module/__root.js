const __root = {
    server: {},
    browser: {},
};

Object.defineProperty(__root, 'server', { get: () => require('./lib/server/server') });
Object.defineProperty(__root, 'browser', { get: () => require('./lib/browser/browser') });

module.exports = __root;
