import * as next from 'next';
import { createServer } from 'http';

import { router } from '../routes';

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = router.getRequestHandler(app);

app.prepare()
.then(() => {
  createServer(handle)
  .listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  })
});
