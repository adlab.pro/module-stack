"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const next = require("next");
const http_1 = require("http");
const routes_1 = require("../routes");
const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = routes_1.router.getRequestHandler(app);
app.prepare()
    .then(() => {
    http_1.createServer(handle)
        .listen(port, (err) => {
        if (err)
            throw err;
        console.log(`> Ready on http://localhost:${port}`);
    });
});
//# sourceMappingURL=index.js.map